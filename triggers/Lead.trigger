/*
** Trigger:  Lead
** SObject:  Lead
** Created by OpFocus on 01/06/2016
** Description: Lead trigger
** + When a Lead is converted to an Opportunity, update any Appointment_Slot__c records that refer to the Lead to 
**   make them also refer to the new Opportunity.
**              
*/
trigger Lead on Lead (after update) {
	// When a Lead is converted to an Opportunity, update any Appointment_Slot__c records that refer to the Lead to 
	// make them also refer to the new Opportunity.
    if (Trigger.isAfter && Trigger.isUpdate) {
        LeadHelper.updateAppointmentSlots(Trigger.newMap, Trigger.oldMap);
    }
}