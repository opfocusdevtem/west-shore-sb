/*
** Class:  TestApptConfirmationCon
** Created by OpFocus on 11/18/2015
** Description: Unit test for Appt Confirmation Controller
*/ 	
@isTest
private class TestApptConfirmationCon {

	private static final Boolean testInit = true;
	private static final Boolean testConfirmAppt = true;
	private static final Boolean testApplyFilters = true;
	private static final Boolean testSelectAppt = true;
	private static final Boolean testChangeDate = true;
	private static final Boolean testReschedulingAppt = true;

	@isTest
	static void testInit() {
		System.assert(testInit, 'Test Disabled.');
		TestApptHelper.initData(false, TestApptHelper.CONFIRMATION, Page.ApptConfirmation);

	    PageReference testPage = Page.ApptConfirmation;
	    Test.setCurrentPageReference(testPage);

		Test.startTest();
		    TestApptHelper.con = new ApptConfirmationCon();
		Test.stopTest();

		// verify that all our appointments were created in a single row
		List<BaseApptCon.TeamAppointmentRow> teamRows = TestApptHelper.con.apptRows.get(TestApptHelper.mapTeamsByType.get('Sales').Id);
		System.assertEquals(1, teamRows.size(), 'Did not create expected number of rows' );
	}


	@isTest
	static void testConfirmAppt() {
		System.assert(testConfirmAppt, 'Test Disabled.');
		TestApptHelper.initData(true, TestApptHelper.CONFIRMATION, Page.ApptConfirmation);

		WestShoreUtils.AppointmentFilters filters = new WestShoreUtils.AppointmentFilters();
		filters.productGroup = 'All';
		filters.region = 'All';
    	filters.apptTypes = 'Sales';
    	filters.startDate = Date.today().format();

		WestShoreUtils.AppointmentWrapper apptDetails = new WestShoreUtils.AppointmentWrapper();
		List<BaseApptCon.TeamAppointmentRow> teamRows = 
			TestApptHelper.con.apptRows.get(TestApptHelper.mapTeamsByType.get('Sales').Id);

		WestShoreUtils.EventWrapper wrapper = teamRows.get(0).appts.get(0);
		apptDetails.id = ((WestShoreUtils.AppointmentWrapper)wrapper).apptSlot.Id;
		apptDetails.selDate = Date.today().format();
		apptDetails.startTime = '9:30';
		apptDetails.duration = 120;
		apptDetails.notes = 'test notes';
		apptDetails.residenceId = String.valueOf(TestApptHelper.lead.Id);
		apptDetails.status = 'Available';
		apptDetails.type = wrapper.type;
		apptDetails.subType = wrapper.subType;
		apptDetails.team = wrapper.team;
		apptDetails.addlTeam = TestApptHelper.mapTeamsByType.get('Install').Id;
		apptDetails.addlTeamName = 'Install Team';
		apptDetails.buffer = 90;
		apptDetails.requestedStatus = WestShoreUtils.CONFIRMED;
		apptDetails.requestedStatusAction = 'Confirming';
		apptDetails.productGroup = 'Windows & Doors';
		apptDetails.attemptedToContact = false;
		apptDetails.numQuarterHours = 14;
		apptDetails.region = TestApptHelper.mechanicsburg.Id;
		apptDetails.regionName = TestApptHelper.mechanicsburg.Name;
		
		
		BaseApptCon.SelectedApptWrapper selWrapper = new BaseApptCon.SelectedApptWrapper(apptDetails.Id, 
			apptDetails.requestedStatus, TestApptHelper.mapTeamsByType.get('Install').Id, 'Sales',
			TestApptHelper.mechanicsburg.Id);

		WestShoreUtils.RemoteActionResults results = null;

		Test.startTest();
			results = ApptConfirmationCon.updateAppts(JSON.serialize(filters),
				JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>{selWrapper}));
		Test.stopTest();

		System.assertEquals(true, results.isSuccess, 
			'Did not confirm Appointment correctly as expected. Error Details = ' + results.details);
	}

	@isTest
	static void testReschedulingAppt() {
		System.assert(testReschedulingAppt, 'Test disabled.');
		TestApptHelper.initData(true, TestApptHelper.CONFIRMATION, Page.ApptConfirmation);	
			
		WestShoreUtils.AppointmentFilters filters = new WestShoreUtils.AppointmentFilters();
		filters.productGroup = 'All';
		filters.region = 'All';
    	filters.apptTypes = 'Sales';
    	filters.startDate = Date.today().format();

		List<BaseApptCon.TeamAppointmentRow> teamRows = 
			TestApptHelper.con.apptRows.get(TestApptHelper.mapTeamsByType.get('Sales').Id);

		WestShoreUtils.AppointmentWrapper apptDetails = 
			(WestShoreUtils.AppointmentWrapper)(teamRows.get(0).appts.get(0));
		apptDetails.requestedStatus = WestShoreUtils.SETT;
		apptDetails.requestedStatusAction = 'Setting';
		apptDetails.productGroup = 'Windows & Doors';
		apptDetails.region = TestApptHelper.mechanicsburg.Id;

		BaseApptCon.SelectedApptWrapper selWrapper = new BaseApptCon.SelectedApptWrapper(apptDetails.Id, 
			apptDetails.requestedStatus, TestApptHelper.mapTeamsByType.get('Sales').Id, 'Sales',
			TestApptHelper.mechanicsburg.Id);
		// first set an appointment
		WestShoreUtils.RemoteActionResults results = ApptConfirmationCon.updateAppts(JSON.serialize(filters),
				JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>{selWrapper}));
	
		// now reschedule it - need id for appointment we just set
		Id apptId = [select Id from Appointment_Slot__c where Status__c='Set' limit 1].Id;
		apptDetails.id = apptId;
		apptDetails.isBeingRescheduled = true;
		apptDetails.rescheduledDate = Date.today().addDays(7).format();
		apptDetails.rescheduledTime = '10:30';
		apptDetails.rescheduledStatus = WestShoreUtils.CONFIRMED;

		selWrapper = new BaseApptCon.SelectedApptWrapper(apptDetails.Id, 
			apptDetails.requestedStatus, TestApptHelper.mapTeamsByType.get('Sales').Id, 'Sales',
			TestApptHelper.mechanicsburg.Id);

		Test.startTest();
			results = BaseApptCon.selectAppt(
				JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>{selWrapper}), 
         		JSON.serialize(apptDetails), JSON.serialize(filters), false);
		Test.stopTest();

		System.assertEquals(true, results.isSuccess, 
			'Did not confirm Appointment correctly as expected. Error Details = ' + results.details);
	}
	
	@isTest
	static void testApplyFilters() {
		System.assert(testApplyFilters, 'Test Disabled.');
		TestApptHelper.initData(true, TestApptHelper.CONFIRMATION, Page.ApptConfirmation);
		TestApptHelper.testApplyFilters(TestApptHelper.CONFIRMATION, Page.ApptConfirmation);
	}

	@isTest
	static void testChangeDate() {
		System.assert(testChangeDate, 'Test Disabled.');
		TestApptHelper.initData(false, TestApptHelper.CONFIRMATION, Page.ApptConfirmation);
		TestApptHelper.testChangeDate(TestApptHelper.CONFIRMATION, Page.ApptConfirmation, 'Sales');
	}

	@isTest
	static void testSelectAppt() {
		System.assert(testSelectAppt, 'Test Disabled.');
		TestApptHelper.initData(true, TestApptHelper.CONFIRMATION, Page.ApptConfirmation);
		TestApptHelper.testSelectAppt(TestApptHelper.CONFIRMATION, Page.ApptConfirmation, 
			WestShoreUtils.CONFIRMED, 'Confirming');
	}
}