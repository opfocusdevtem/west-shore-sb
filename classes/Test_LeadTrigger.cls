/*
** Class:  Test_LeadTrigger
** Created by OpFocus on 01/06/2016
** Description: Unit tests for the LeadHelper class and Lead trigger
**              
*/  
@isTest
private class Test_LeadTrigger {

    private static Boolean testLeadTrigger = true;
    
    private static testMethod void testLeadTrigger() {
        System.assert(testLeadTrigger, 'Test disabled');
        
        // Create a Lead with an Appointment Slot and another without one
        Lead ldWithSlot = createLead('Lead with Slot');
        Lead ldNoSlot = createLead('Lead without Slot');
        insert new List<Lead>{ldWithSlot, ldNoSlot};
        
        Appointment_Slot__c slot = createAppointmentSlot(ldWithSlot);
        insert slot;
        
        // Convert the Leads
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        
		Database.LeadConvert lc1 = new Database.LeadConvert();
		lc1.setLeadId(ldWithSlot.id);
		lc1.setConvertedStatus(convertStatus.MasterLabel);

		Database.LeadConvert lc2 = new Database.LeadConvert();
		lc2.setLeadId(ldNoSlot.id);
		lc2.setConvertedStatus(convertStatus.MasterLabel);

        Database.convertLead(new List<Database.LeadConvert>{lc1, lc2});
        System.assertEquals(2, [select count() from Lead where IsConverted = true], 'Both Leads should have been converted');
        
        System.assertEquals(1, [select count() from Appointment_Slot__c where Lead__c = null and Opportunity__c != null], 
            'Slot does not refer to Opp');
    }
    
    
    // Create a Lead
    private static Lead createLead(String lastName) {
        Lead ld = new Lead();
        ld.LastName = lastName;
        ld.Company = lastName;
        ld.Appointment_Date__c = Date.today();
        return ld;
    }
    
	// Create an Appointment Slot for the given Lead
    private static Appointment_Slot__c createAppointmentSlot(Lead ld) {
		Appointment_Slot__c slot = new Appointment_Slot__c();
        slot.Start_Time__c = DateTime.now();
        slot.Lead__c = ld.Id;
        slot.Status__c = 'Set';
        return slot;
    }
}