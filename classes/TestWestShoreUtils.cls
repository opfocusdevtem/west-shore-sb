@isTest
private class TestWestShoreUtils
{
	private static final Boolean testGetResultReasons = true;
	private static final Boolean testGetTimeValues = true;
	private static final Boolean testGetDurationValues = true;
	private static final Boolean testGetSubTypes = true;
	private static final Boolean testGetStatuses = true;
	private static final Boolean testJsonData = true;
	private static final Boolean testPTOWrapper = true;
	private static final Boolean testApptsOutsideHours = true;

	@isTest
	static void testJsonData() {
		System.assert(testJsonData, 'Test disabled.');
		initData(false);

		WestShoreUtils.EventWrapper wrapper = null;
		Appointment_Slot__c appt = lstAppts.get(0); // gt a test appointment to use

		Test.startTest();
			wrapper = new WestShoreUtils.AppointmentWrapper();
			wrapper.isPlaceHolder = false;
			wrapper.selDate = Date.today().format();
			wrapper.id = appt.Id;
    		wrapper.startTime = '9:00';
    		wrapper.duration = 60;
    		wrapper.setResidence(acct);
    		wrapper.notes = 'notes';
    		wrapper.status = 'Status';
    		wrapper.type = 'type';
    		wrapper.subType = 'subType';
    		wrapper.requestedTeam = lstTeams.get(0).Id;
    		wrapper.requestedTeamName =  'Team Name';
    		wrapper.productGroup = 'Product group';
    		wrapper.region = testRegion.Id;
    		wrapper.regionName = 'region name';
    		wrapper.requestedStatus = 'requested Status';
    		wrapper.requestedStatusAction = 'requested Status action';
    		wrapper.numQuarterHours = 4;
    		wrapper.buffer = 90;
    		wrapper.totalDuration = 150;
    		wrapper.resultReason = 'result';
    		wrapper.isAccount = true;
    		wrapper.isOneLegger = false;
    		wrapper.numPeople = 1;
    		wrapper.needToBeHome = true;
    		wrapper.name='name';
    		wrapper.nextOppStage = 'Confirming';
    		wrapper.nextDueDate = Date.today().addDays(7).format();
    		
    		String jsonData = wrapper.jsonData;
		Test.stopTest();

		System.assertEquals(true, wrapper.isAccount, 'isAccount did not return true when it should.');
		System.assert(jsonData != null, 'jsonData returned null.');
	}
	
	@isTest
	static void testPTOWrapper() {
		System.assert(testPTOWrapper, 'Test disabled.');
		initData(true);

		Team__c team = new Team__c(Type__c='Sales', Name='Sales Team');
		insert team;

		User testUser = TestUtils.createUser('SalesRep', team.Name, 'User');
		insert testUser;

		// Create a PTO event for our wrapper
		Event testEvent = new Event(ActivityDate=Date.today(), 
			StartDateTime= Datetime.newInstance(Date.today().year(), Date.today().month(), Date.today().day(),
				14,0,0), 
			EndDateTime=Datetime.newInstance(Date.today().year(), Date.today().month(), Date.today().day(),
				18,0,0),
			DurationInMinutes=240,
			Subject='Beach Vacation', Type='PTO', OwnerId=testUser.Id);
		insert testEvent;

		WestShoreUtils.PTOWrapper ptoWrapper;
		Test.startTest();
			ptoWrapper = new WestShoreUtils.PTOWrapper(con, testEvent, team);
		Test.stopTest();

		// verify that we set fields correctly
		System.assertEquals('PTO', ptoWrapper.Status, 'Failed to set fields on PTOWrapper correctly.');
		System.assertEquals('6:00', ptoWrapper.apptEndTime, 'Failed to convert from 24-hour clock to 12-hour clock');
	}

	@isTest
	static void testGetTimeValues() {
		System.assert(testGetTimeValues, 'Test disabled.');

		Test.startTest();
		initData(false);
		Test.stopTest();
		System.assertEquals(0, WestShoreUtils.getTimeValues(new String[]{'Install'}).size(), 
			'Returned Time values when none should exist');

		System.assertEquals(9, WestShoreUtils.getTimeValues(new String[]{'Sales'}).size(), 
			'Failed to return Time values as expected.');
	}

	@isTest
	static void testGetResultReasons() {
		System.assert(testGetResultReasons, 'Test disabled.');

		Test.startTest();
		initData(true);
		Test.stopTest();
		System.assert(WestShoreUtils.getResultReasons().size() > 0, 
			'Failed to return Result Reasons values as expected.');
	}


	@isTest
	static void testGetDurationValues() {
		System.assert(testGetDurationValues, 'Test disabled.');
		initData(false);
		System.assertEquals(0, WestShoreUtils.getDurationValues(new String[]{'Sales'}).size(), 
			'Returned duration values when none exist');

		// now create them 
		List<Appt_Duration_Values__c> lstDurationValues = new List<Appt_Duration_Values__c>();
		lstDurationValues.add(new Appt_Duration_Values__c(Name='90 minutes', Appt_Type__c='Sales',
			Label__c='90 minutes', Value__c='90'));
		lstDurationValues.add(new Appt_Duration_Values__c(Name='60 minutes', Appt_Type__c='Sales',
			Label__c='60 minutes', Value__c='60'));
		lstDurationValues.add(new Appt_Duration_Values__c(Name='30 minutes', Appt_Type__c='Sales',
			Label__c='30 minutes', Value__c='30'));
		lstDurationValues.add(new Appt_Duration_Values__c(Name='120 minutes', Appt_Type__c='Sales',
			Label__c='120 minutes', Value__c='120'));
		insert lstDurationValues;
		System.assertEquals(4, WestShoreUtils.getDurationValues(new String[]{'Sales'}).size(), 
			'Failed to return duration values as expected.');
	}


	@isTest
	static void testGetStatuses() {
		System.assert(testGetStatuses, 'Test disabled.');
		initData(false);
		System.assertEquals(0, WestShoreUtils.getStatuses(new String[]{'Install'}).size(), 
			'Returned status values when none exist');

		// now create them 
		List<Status__c> lstStatuses = new List<Status__c>();
		lstStatuses.add(new Status__c(Name='1', Label__c='Set', Value__c='Set', Appt_Type__c='Sales'));
		lstStatuses.add(new Status__c(Name='2', Label__c='Confirmed', Value__c='Confirmed', Appt_Type__c='Sales'));
		lstStatuses.add(new Status__c(Name='3', Label__c='Staffed', Value__c='Staffed', Appt_Type__c='Sales'));
		lstStatuses.add(new Status__c(Name='4', Label__c='Canceled', Value__c='Canceled', Appt_Type__c='Sales'));
		insert lstStatuses;
		System.assertEquals(4, WestShoreUtils.getStatuses(new String[]{'Sales'}).size(), 
			'Failed to return status values as expected.');
	}

	@isTest
	static void testGetSubTypes() {
		System.assert(testGetSubTypes, 'Test disabled.');
		initData(false);
		System.assertEquals(0, WestShoreUtils.getSubTypes(new String[]{'Sales'}).size(), 
			'Returned Sub Type values when none exist');

		// now create them 
		List<Sub_Type__c> lstSubTypes = new List<Sub_Type__c>();
		lstSubTypes.add(new Sub_Type__c(Name='1', Appt_Type__c='All',
			Label__c='SubType 1', Value__c='SubType 1'));
		lstSubTypes.add(new Sub_Type__c(Name='2', Appt_Type__c='All',
			Label__c='SubType 2', Value__c='SubType 2'));
		lstSubTypes.add(new Sub_Type__c(Name='3', Appt_Type__c='All',
			Label__c='SubType 3', Value__c='SubType 3'));
		lstSubTypes.add(new Sub_Type__c(Name='4', Appt_Type__c='All',
			Label__c='SubType 4', Value__c='SubType 4'));
		insert lstSubTypes;
		System.assertEquals(4, WestShoreUtils.getSubTypes(new String[]{'Sales'}).size(), 
			'Failed to return subtype values as expected.');
	}
	
	@isTest
	static void testApptsOutsideHours() {
		System.assert(testApptsOutsideHours, 'test disabled.');
		initData(true);

		Team__c salesTeam;
		for (Team__c team : lstTeams) {
			if (team.Type__c.equals('Sales')) {
				salesTeam = team;
				break;
			}
		}
		// add an appointment with an end date past our last hour
		Appointment_Slot__c appt = new Appointment_Slot__c(Start_Time__c = 
			DateTime.newInstance(Date.today(), 
			Time.newInstance(10,00,0,0)), Duration__c = 90, Standard_Buffer__c = 90,
			Team__c = salesTeam.Id, Account__c = acct.Id, 
			Status__c = 'Available', Type_of_Appointment__c = salesTeam.Type__c);
		insert appt;

		WestShoreUtils.AppointmentWrapper wrapper = new WestShoreUtils.AppointmentWrapper(con, appt);

		// now verify that time has been pushed back but appt time to display is the same
		System.assertEquals('11:30', wrapper.apptEndTime, 'Appointment Time not left alone as it should be.');
		System.assertEquals('11:00',  wrapper.endTime, 'Appointment Time not adjusted.');

		// add an appointment with an start & end date past our last hour
		appt = new Appointment_Slot__c(Start_Time__c = 
			DateTime.newInstance(Date.today(), 
			Time.newInstance(12,00,0,0)), Duration__c = 90, Standard_Buffer__c = 90,
			Team__c = salesTeam.Id, Account__c = acct.Id, 
			Status__c = 'Available', Type_of_Appointment__c = salesTeam.Type__c);
		insert appt;

		wrapper = new WestShoreUtils.AppointmentWrapper(con, appt);

		// now verify that startTime has been pushed back but appt startTime to display is the same
		System.assertEquals('12:00', wrapper.apptStartTime, 'Appointment Start Time not left alone as it should be.');
		System.assertEquals('10:45',  wrapper.startTime, 'Start Time not adjusted.');
		System.assertEquals('1:30', wrapper.apptEndTime, 'Appointment End Time not left alone as it should be.');
		System.assertEquals('11:00',  wrapper.endTime, 'End Time not adjusted.');

		// add an appointment with an start date before our first hour
		appt = new Appointment_Slot__c(Start_Time__c = 
			DateTime.newInstance(Date.today(), 
			Time.newInstance(7,00,0,0)), Duration__c = 90, Standard_Buffer__c = 90,
			Team__c = salesTeam.Id, Account__c = acct.Id, 
			Status__c = 'Available', Type_of_Appointment__c = salesTeam.Type__c);
		insert appt;

		wrapper = new WestShoreUtils.AppointmentWrapper(con, appt);

		// now verify that startTime has been pushed back but appt startTime to display is the same
		System.assertEquals('7:00', wrapper.apptStartTime, 'Appointment Start Time not left alone as it should be.');
		System.assertEquals('9:00',  wrapper.startTime, 'Start Time not adjusted.');

		// add an appointment with a start date before our first hour
		appt = new Appointment_Slot__c(Start_Time__c = 
			DateTime.newInstance(Date.today(), 
			Time.newInstance(8,00,0,0)), Duration__c = 90, Standard_Buffer__c = 60,
			Team__c = salesTeam.Id, Account__c = acct.Id, 
			Status__c = 'Available', Type_of_Appointment__c = salesTeam.Type__c);
		insert appt;

		wrapper = new WestShoreUtils.AppointmentWrapper(con, appt);

		// now verify that startTime has been pushed back but appt startTime to display is the same
		System.assertEquals('8:00', wrapper.apptStartTime, 'Appointment Start Time not left alone as it should be.');
		System.assertEquals('9:00',  wrapper.startTime, 'Start Time not adjusted.');
		System.assertEquals('9:30', wrapper.apptEndTime, 'Appointment End Time not left alone as it should be.');

		// add an appointment with a start & end date before our first hour
		appt = new Appointment_Slot__c(Start_Time__c = 
			DateTime.newInstance(Date.today(), 
			Time.newInstance(7,00,0,0)), Duration__c = 30, Standard_Buffer__c = 15,
			Team__c = salesTeam.Id, Account__c = acct.Id, 
			Status__c = 'Available', Type_of_Appointment__c = salesTeam.Type__c);
		insert appt;

		wrapper = new WestShoreUtils.AppointmentWrapper(con, appt);

		// now verify that startTime has been pushed back but appt startTime to display is the same
		System.assertEquals('7:00', wrapper.apptStartTime, 'Appointment Start Time not left alone as it should be.');
		System.assertEquals('9:00',  wrapper.startTime, 'Start Time not adjusted.');
		System.assertEquals('7:30', wrapper.apptEndTime, 'Appointment End Time not left alone as it should be.');
		System.assertEquals('9:15',  wrapper.endTime, 'End Time not adjusted.');
	}

	private static void initData(Boolean blnCreateCtrl) {
		Account wsAcct = TestUtils.createVendor('West Shore, Inc');
		insert wsAcct;

		List<Contact> lstContacts = new List<Contact>();
		lstContacts.add(TestUtils.createContact(wsAcct, 'First', 'Last', '7035551212',
			'100 some Street', 'City', 'Maryland', '012345'));
		lstContacts.add(TestUtils.createContact(wsAcct, 'Second', 'Last', '7035551212',
			'100 some Street', 'City', 'Delaware', '012345'));
		lstContacts.add(TestUtils.createContact(wsAcct, 'Third', 'Last', '7035551212',
			'100 some Street', 'City', 'Maine', '012345'));
		insert lstContacts;

		for (Contact contact : lstContacts) {
			System.debug('==========> contact = ' + contact);
		}
		config = new Scheduler_Config__c(Name = 'Scheduler Config', 
			Conflicting_Event_Types__c='Company Event,Company Holiday,PTO Request',
			Buffer_Time__c=90, Excluded_Statuses__c='Canceled,Rescheduled');
		insert config;
		
		// create Time Values 
		List<Appt_Time_Values__c> lstTimeValues = new List<Appt_Time_Values__c>();
		lstTimeValues.add(new Appt_Time_Values__c(Name='1', Appt_Type__c='Sales',
			Label__c='9:00 AM', Value__c='9:00'));
		lstTimeValues.add(new Appt_Time_Values__c(Name='2', Appt_Type__c='Sales',
			Label__c='9:15 AM', Value__c='9:15'));
		lstTimeValues.add(new Appt_Time_Values__c(Name='3', Appt_Type__c='Sales',
			Label__c='9:30 AM', Value__c='9:30'));
		lstTimeValues.add(new Appt_Time_Values__c(Name='4', Appt_Type__c='Sales',
			Label__c='9:45 AM', Value__c='9:45'));
		lstTimeValues.add(new Appt_Time_Values__c(Name='5', Appt_Type__c='Sales',
			Label__c='10:00 AM', Value__c='10:00'));
		lstTimeValues.add(new Appt_Time_Values__c(Name='6', Appt_Type__c='Sales',
			Label__c='10:15 AM', Value__c='10:15'));
		lstTimeValues.add(new Appt_Time_Values__c(Name='7', Appt_Type__c='Sales',
			Label__c='10:30 AM', Value__c='10:30'));
		lstTimeValues.add(new Appt_Time_Values__c(Name='8', Appt_Type__c='Sales',
			Label__c='10:45 AM', Value__c='10:45'));
		lstTimeValues.add(new Appt_Time_Values__c(Name='9', Appt_Type__c='Sales',
			Label__c='11:00 AM', Value__c='11:00', Is_Last_Hour__c=true));
		insert lstTimeValues;


		// create Team Lead Users
		User user1 = TestUtils.createUser('Sales1', 'Team', 'User1');
		User user2 = TestUtils.createUser('Sales2', 'Team', 'User2');
		insert new List<User>{user1, user2};

		lstTeams = new List<Team__c>();
		Team__c team1 = new Team__c(Name ='Sales Team 1', Type__c ='Sales', User__c=user1.Id);
		lstteams.add(team1);
		Team__c team2 = new Team__c(Name ='Sales Team 2', Type__c ='Sales', User__c=user2.Id);
		lstteams.add(team2);

		Team__c team3 = new Team__c(Name ='Set & Unassigned', Type__c ='Set & Unassigned');
		lstteams.add(team3);
		Team__c team4 = new Team__c(Name ='Confirmed & Unassigned', Type__c ='Confirmed & Unassigned');
		lstteams.add(team4);
		
		insert lstTeams;


		// create a test account
		acct = TestUtils.createAccount('Test Account');
		insert acct;

		Contact contact = TestUtils.createContact(acct, 'First', 'Last', '7035551212',
			'100 Broadway', 'New York', 'New York', '01234');
		contact.Primary_Contact__c = true;
		insert contact;

		// and a test Lead 
		lead = TestUtils.createLead('Test', 'Lead', 'Company');
		insert lead;

		Date myDate = Date.today();
		for (Team__c team : lstTeams){
			// now create some appointments 
			Integer hour = 9;
			for (Integer minute = 0; minute < 60; minute += 30) {
				lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, 
					Time.newInstance(hour,minute,0,0)), 
					Team__c = team.Id, Account__c = acct.Id, 
					Status__c = 'Available', Type_of_Appointment__c = team.Type__c));
				lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, 
					Time.newInstance(hour+4,minute,0,0)),
					Team__c = team.Id, Account__c = acct.Id,
					Status__c = 'Available', Type_of_Appointment__c = team.Type__c));
				lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, 
					Time.newInstance(hour+4,minute,0,0)),
					Team__c = team.Id, Account__c = acct.Id,
					Status__c = 'Available', Type_of_Appointment__c = team.Type__c));
				hour = 10;
			}
		}
		insert lstAppts;

		Event testEvent = new Event(ActivityDate=Date.today(), 
			ActivityDateTime=DateTime.newInstance(Date.today(), Time.newInstance(9,0,0,0)),
			DurationInMinutes=480,
			Subject='Beach Vacation', Type='PTO', OwnerId=user1.Id);
		insert testEvent;

		testRegion = new Region__c(name='test region');
		insert testRegion;

		if (blnCreateCtrl) {
		    PageReference testPage = Page.ApptScheduler;
		    Test.setCurrentPageReference(testPage);
	    	ApexPages.currentPage().getParameters().put('id', acct.Id);
		    con = new ApptSchedulerCon();
		}
	}

	private static List<Team__c> lstTeams = null;
	private static ApptSchedulerCon con = null;

	private static Scheduler_Config__c config = null;
	private static Account acct = null;
	private static Lead lead = null;
	private static Region__c testRegion = null;
	private static List<Appointment_Slot__c> lstAppts = new List<Appointment_Slot__c>();

}