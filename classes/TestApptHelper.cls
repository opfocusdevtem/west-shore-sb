/*
** Class:  TestApptHelper
** Created by OpFocus on 1/9/2016
** Description: Helper methods for Appt Scheduler & Appt Confirmation controllers
**/
public class TestApptHelper {
	public static void testChangeDate(String controllerName, PageReference pageRef) {
		TestApptHelper.testChangeDate(controllerName, pageRef, 'Sales');
	}
	
	public static void testChangeDate(String controllerName, PageReference pageRef, String apptType) {
		// now change the date for which we are getting appointments
		Date tomorrow = Date.today().addDays(1);

		WestShoreUtils.AppointmentFilters filters = new WestShoreUtils.AppointmentFilters();
		filters.productGroup = 'All';
		filters.region = 'All';
    	filters.residenceId = lead.Id;
    	filters.apptTypes = 'Sales';
    	filters.startDate = tomorrow.format();

    	Test.startTest();
			WestShoreUtils.RemoteActionResults results = null;
			if (controllerName.equals(SCHEDULER)) {
				results = 
					ApptSchedulerCon.changeDate(JSON.serialize(filters), 
						JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()));
			} else if (controllerName.equals(ACCT_SCHEDULER)) {
				results = 
					ApptSchedulerAcctCon.changeDateForAcct(JSON.serialize(filters), 
						JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()));
			} else if (controllerName.equals(CONFIRMATION)) {
				results = 
					ApptConfirmationCon.changeDate(JSON.serialize(filters), 
						JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()));	
			} else if (controllerName.equals(OPER_CONFIRMATION)) {
				results = 
					OperApptConfirmationCon.changeDate(JSON.serialize(filters), 
						JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()));	
			}

			System.assert(results.details.contains('&ourDate='), 
				'Results details = ' + results.details + 
				' do not contain start date parameter as expected.');

			// now construct controller with new dates
			// first set Page parameters
		    PageReference testPage = pageRef;
		    Test.setCurrentPageReference(testPage);
		    ApexPages.currentPage().getParameters().put('ourDate', tomorrow.format());
	    	if (controllerName.equals(SCHEDULER)) {
		    	ApexPages.currentPage().getParameters().put('id', acct.Id);  
				con = new ApptSchedulerCon();
	    	} else if (controllerName.equals(ACCT_SCHEDULER)){ 
		    	ApexPages.currentPage().getParameters().put('id', acct.Id);
			    con = new  ApptSchedulerAcctCon(new ApexPages.StandardController(acct));
			} else if (controllerName.equals(CONFIRMATION)){
				con = new ApptConfirmationCon();
	    	} else if (controllerName.equals(OPER_CONFIRMATION)) {
				con = new OperApptConfirmationCon();
	    	}

		Test.stopTest();

		// should be 1 row for team as we did not create conflicting appointments
		System.assertEquals(1, con.apptRows.get(mapTeamsByType.get(apptType).Id).size(), 
			'Did not create correct number of appointment rows when the date was changed.');
	}

	public static void testApplyFilters(String controllerName, PageReference pageRef) {
		WestShoreUtils.AppointmentFilters filters = new WestShoreUtils.AppointmentFilters();
		filters.productGroup = 'WindowsDoors';
		filters.region = 'All';
    	filters.residenceId = lead.Id;
    	filters.apptTypes = 'Sales';
    	filters.startDate = Date.today().format();

		WestShoreUtils.RemoteActionResults results = null;

		Test.startTest();
			if (controllerName.equals(SCHEDULER)) {
				results = ApptSchedulerCon.applyFilters(JSON.serialize(filters), 
					JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()));
	    	} else if (controllerName.equals(ACCT_SCHEDULER)){ 
				results = ApptSchedulerAcctCon.applyFiltersForAcct(JSON.serialize(filters), 
					JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()));
			} else if (controllerName.equals(CONFIRMATION)){
				results = ApptConfirmationCon.applyFilters(JSON.serialize(filters), 
					JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()));
	    	} else if (controllerName.equals(OPER_CONFIRMATION)) {
				results = OperApptConfirmationCon.applyFilters(JSON.serialize(filters), 
					JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()));
	    	}
		Test.stopTest();
		System.assertEquals(results.isSuccess, true, 'applyFilters did not return true as it should.');
		System.assert(results.details.containsIgnoreCase('WindowsDoors'), 
			'applyFilters did not return a URL containing Product Group value as it should.');
	}

	public static void testSelectAppt(String controllerName, PageReference pageRef, String requestedStatus,
			String requestedStatusAction) {
		TestApptHelper.testSelectAppt(controllerName, pageRef, requestedStatus, 
			requestedStatusAction, 'Sales', 'Sales');
	}
    
    public static void testSelectAppt(String controllerName, PageReference pageRef, String requestedStatus,
			String requestedStatusAction, String teamType, String apptType) {
        WestShoreUtils.AppointmentWrapper apptDetails = new WestShoreUtils.AppointmentWrapper();
        WestShoreUtils.EventWrapper wrapper = 
            con.apptRows.get(mapTeamsByType.get(teamType).Id).get(0).appts.get(0);
        apptDetails.id = ((WestShoreUtils.AppointmentWrapper)wrapper).Id;
        apptDetails.selDate = Date.today().format();
        apptDetails.startTime = '9:30';
        apptDetails.duration = 120;
        apptDetails.numQuarterHours = 14;
        apptDetails.residenceId = lead.Id;
        apptDetails.status = 'Available';
        apptDetails.notes = 'test notes';
        apptDetails.type = wrapper.type;
        apptDetails.subType = wrapper.subType;
        apptDetails.team = wrapper.team;
        apptDetails.buffer = 90;
        apptDetails.requestedStatus = requestedStatus;
        apptDetails.requestedStatusAction = requestedStatusAction;
        apptDetails.productGroup = 'Windows & Doors';
        apptDetails.isOneLegger = false;
        apptDetails.numPeople = 1;
        apptDetails.name = 'name';
        apptDetails.needToBeHome = false;
        apptDetails.region = mechanicsburg.Id;
        apptDetails.regionName = mechanicsburg.Name;
        
        WestShoreUtils.AppointmentFilters filters = new WestShoreUtils.AppointmentFilters();
        filters.productGroup = 'All';
        filters.region = 'All';
        filters.residenceId = lead.Id;
        filters.apptTypes = 'Install';
        filters.startDate = Date.today().format();

        WestShoreUtils.RemoteActionResults results = null;
        List<BaseApptCon.SelectedApptWrapper> lstWrappers = new List<BaseApptCon.SelectedApptWrapper>();
        Test.startTest();
			if (controllerName.equals(SCHEDULER)) {
	            results = ApptSchedulerCon.selectAppt(JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()), 
	                JSON.serialize(apptDetails), 
	                JSON.serialize(filters));
	    	} else if (controllerName.equals(ACCT_SCHEDULER)) {
	            results = ApptSchedulerAcctCon.selectApptForAcct(JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()), 
	                JSON.serialize(apptDetails), 
	                JSON.serialize(filters));
	    	} else if (controllerName.equals(CONFIRMATION)){
	            results = ApptConfirmationCon.selectAppt(JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()), 
	                JSON.serialize(apptDetails), 
	                JSON.serialize(filters));
	    	} else if (controllerName.equals(OPER_CONFIRMATION)){
	            results = OperApptConfirmationCon.selectAppt(JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()), 
	                JSON.serialize(apptDetails), 
	                JSON.serialize(filters));
	        }

            System.assertEquals(true, results.isSuccess, 
                'Did not select Appointment correctly as expected. Error Details = ' + results.details);
            List<Appointment_Slot__c> lstAppts  = 	[select Id, Team__c from Appointment_Slot__c where 
                Status__c=:WestShoreUtils.SELECTED];

			System.assertEquals(1,lstAppts.size(),
                'Did not save selected appointment in database as expected.');
			System.assertEquals(lstAppts.get(0).Team__c, mapTeamsByType.get('Set & Unassigned (Mechanicsburg)').Id,
                'Did not set Team correctly based on Region of appointment.');
			lstWrappers.add(new BaseApptCon.SelectedApptWrapper([select Id from Appointment_Slot__c where 
                Status__c=:WestShoreUtils.SELECTED limit 1].Id, requestedStatus, 
                wrapper.Team, apptType, mechanicsburg.Id));

			if (controllerName.equals(SCHEDULER)) {
	            results = ApptSchedulerCon.setAppt(JSON.serialize(filters), JSON.serialize(lstWrappers));
	    	}  else if (controllerName.equals(ACCT_SCHEDULER)) {
	    		results = ApptSchedulerAcctCon.setApptForAcct(JSON.serialize(filters), JSON.serialize(lstWrappers));
	    	}  else if (controllerName.equals(CONFIRMATION)){
	            results = ApptConfirmationCon.updateAppts(JSON.serialize(filters), JSON.serialize(lstWrappers));
	    	} else if (controllerName.equals(OPER_CONFIRMATION)){
	            results = ApptConfirmationCon.updateAppts(JSON.serialize(filters), JSON.serialize(lstWrappers));
	    	}
            System.assertEquals(true, results.isSuccess, 
	        	'Did not update Appointment correctly as expected. Error Details = ' + results.details);	    	
        Test.stopTest();
	}

	public static void initData(Boolean blnCreateCtrl, String controllerName, PageReference pageRef) {
		// create West Shore Account and Contacts
		Account wsAcct = TestUtils.createVendor('West Shore, Inc');
		insert wsAcct;

		lstContacts = new List<Contact>();
		lstContacts.add(TestUtils.createContact(wsAcct, 'First', 'Last', '7035551212',
			'100 some Street', 'City', 'Virginia', '012345'));
		lstContacts.add(TestUtils.createContact(wsAcct, 'Second', 'Last', '7035551212',
			'100 some Street', 'City', 'Virginia', '012345'));
		lstContacts.add(TestUtils.createContact(wsAcct, 'Third', 'Last', '7035551212',
			'100 some Street', 'City', 'Virginia', '012345'));

		insert lstContacts;

		config = new Scheduler_Config__c(Name = 'Scheduler Config', 
			Conflicting_Event_Types__c='Company Event,Company Holiday,PTO Request',
			Buffer_Time__c=90, Excluded_Statuses__c='Canceled,Rescheduled');
		insert config;
		
		List<Appt_Time_Values__c> lstTimeValues = new List<Appt_Time_Values__c>();
		Integer idx = 0;
		for (Integer startHour = 9; startHour < 17; startHour++) {
			for (Integer minutes = 0; minutes < 60; minutes += 15) {
				String dateString = startHour + ':' + minutes + (minutes == 0 ? '0' :'');
				String name = 'Time' + (idx > 9 ? '_' : '') + idx;
				lstTimeValues.add(new Appt_Time_Values__c(Name=name, Appt_Type__c='All',
					Label__c=dateString, Value__c=dateString));		
				idx++;		
			}
		}	
		insert lstTimeValues;

		// create regions for our teams and appointments
		mechanicsburg = new Region__c(Name='Mechanicsburg');
		pittsburgh = new Region__c(Name='Pittsburgh');
		insert new List<Region__c> {mechanicsburg, pittsburgh};

		// create our teams
		mapTeamsByType = new Map<String, Team__c>();
		mapTeamsByType.put('Sales', new Team__c( Name ='Sales Team', Type__c ='Sales', Region__c=mechanicsburg.Id));
		mapTeamsByType.put('Install', new Team__c(Name ='Install Team', Type__c ='Install', Region__c=mechanicsburg.Id));
		mapTeamsByType.put('Service', new Team__c(Name ='Service Team', Type__c ='Service',Region__c=mechanicsburg.Id));
		mapTeamsByType.put('Set & Unassigned (Mechanicsburg)', new Team__c(Name ='Set & Unassigned (Mechanicsburg)', 
			Type__c ='Set & Unassigned', Region__c=mechanicsburg.Id));
		mapTeamsByType.put('Set & Unassigned (Pittsburgh)', new Team__c(Name ='Set & Unassigned (Pittsburgh)', Type__c ='Set & Unassigned', 
			Region__c=pittsburgh.Id));
		mapTeamsByType.put('Confirmed & Unassigned (Mechanicsburg)', new Team__c(Name ='Confirmed & Unassigned (Mechanicsburg)', 
			Type__c ='Confirmed & Unassigned', Region__c=mechanicsburg.Id));
		mapTeamsByType.put('Confirmed & Unassigned (Pittsburgh)', new Team__c(Name ='Confirmed & Unassigned (Pittsburgh)', 
			Type__c ='Confirmed & Unassigned', Region__c=pittsburgh.Id));
		insert mapTeamsByType.values();

		// create an associated User for each Team
		mapUsersByTeam = new Map<String, User>();
		for (Team__c team : mapTeamsByType.values()) {
			mapUsersByTeam.put(team.Type__c, TestUtils.createUser(team.Type__c.substring(0,3) + 'User', 
				team.Name, 'User'));
		}
		insert mapUsersByTeam.values();

		// now add User to team members
		for (Team__c team : mapTeamsByType.values()) {
			team.User__c = mapUsersByTeam.get(team.Type__c).Id;
		}
		update mapTeamsByType.values();

		// create a test account
		acct = TestUtils.createAccount('Test Account');
		insert acct;

		// and a test Lead 
		lead = TestUtils.createLead('Test', 'Lead', 'Company');
		insert lead;

		if (blnCreateCtrl) {
			// now create an event for our calendar
			Event testEvent = new Event(ActivityDate=Date.today(), 
				ActivityDateTime=Datetime.now(), 
				DurationInMinutes=480,
				Subject='Beach Vacation', Type='PTO', OwnerId=mapUsersByTeam.get('Sales').Id);
			insert testEvent;

			Date myDate = Date.today();
			for (Team__c team : mapTeamsByType.values()) {
				String apptType = team.Type__c.equals('Sales') ? team.Type__c : 'Operations';
				lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, Time.newInstance(9,0,0,0)), 
					Team__c = team.Id, Account__c = acct.Id, 
					Status__c = 'Set', Type_of_Appointment__c = apptType));
				lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, Time.newInstance(14,0,0,0)),
					Team__c = team.Id, Account__c = acct.Id,
					Status__c = 'Set', Type_of_Appointment__c = apptType));
				lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, Time.newInstance(18,0,0,0)),
					Team__c = team.Id, Account__c = acct.Id,
					Status__c = 'Set', Type_of_Appointment__c = apptType));
			}
			insert lstAppts;

		    Test.setCurrentPageReference(pageRef);

			if (controllerName.equals(SCHEDULER)) {
		    	ApexPages.currentPage().getParameters().put('id', acct.Id);
			    con = new  ApptSchedulerCon();
			} else if (controllerName.equals(ACCT_SCHEDULER)){ 
		    	ApexPages.currentPage().getParameters().put('id', acct.Id);
			    con = new  ApptSchedulerAcctCon(new ApexPages.StandardController(acct));
			} else if (controllerName.equals(CONFIRMATION)) {
			    con = new  ApptConfirmationCon();				
			} else if (controllerName.equals(OPER_CONFIRMATION)) {
			    con = new  OperApptConfirmationCon();				
			}
		}
	}

	public static final String SCHEDULER = 'Scheduler';
	public static final String ACCT_SCHEDULER = 'Account Scheduler';
	
	public static final String CONFIRMATION = 'Confirmation';
	public static final String OPER_CONFIRMATION = 'Operation Confirmation';

	public static Scheduler_Config__c config = null;
	public static Map<String, Team__c> mapTeamsByType = null;
	public static Map<String, User> mapUsersByTeam  = null;
	public static List<Contact> lstContacts;

	public static Region__c mechanicsburg;
	public static Region__c pittsburgh;
	
	public static Account acct;
	public static Lead lead;
	public static BaseApptCon con;
	private static Contact primaryResident = null;
	private static List<Appointment_Slot__c> lstAppts = new List<Appointment_Slot__c>();

}