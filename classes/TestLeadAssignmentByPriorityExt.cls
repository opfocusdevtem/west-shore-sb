@isTest
private class TestLeadAssignmentByPriorityExt {

    static testMethod void leadAssignmentByPriorityExt_ut1() {
        // Create a Lead with an Appointment Slot and another without one
        Lead ldWithSlot = createLead('Lead with Slot');
        Lead ldNoSlot = createLead('Lead without Slot');
        
        Id queueId = [Select Id from Group where DeveloperName = :System.Label.LeadQueue and Type = 'Queue'].Id;
        
        ldWithSlot.OwnerId = queueId;
        ldNoSlot.OwnerId = queueId;
        
        insert new List<Lead>{ldWithSlot, ldNoSlot};
        Test.startTest();
        	Apexpages.StandardController testCon = new ApexPages.StandardController(ldWithSlot);
        	LeadAssignmentByPriorityExt testExt = new LeadAssignmentByPriorityExt(testCon);
        	testExt.redirect();
        Test.stopTest();
            
    }

    static testMethod void leadAssignmentByPriorityExt_ut2() {
        // Create a Lead with an Appointment Slot and another without one
        Lead ldWithSlot = createLead('Lead with Slot');
        Lead ldNoSlot = createLead('Lead without Slot');
        
        Id queueId = [Select Id from Group where DeveloperName = :System.Label.LeadQueue and Type = 'Queue'].Id;
        
        ldWithSlot.OwnerId = queueId;
        ldNoSlot.OwnerId = queueId;
        ldNoSlot.Next_Activity_Tier__c = '';
        ldWithSlot.Next_Activity_Tier__c = '';
        
        insert new List<Lead>{ldWithSlot, ldNoSlot};
        Test.startTest();
            Apexpages.StandardController testCon = new ApexPages.StandardController(ldWithSlot);
            LeadAssignmentByPriorityExt testExt = new LeadAssignmentByPriorityExt(testCon);
            testExt.redirect();
        Test.stopTest();
            
    }

    static testMethod void leadAssignmentByPriorityExt_ut3() {
        // Create a Lead with an Appointment Slot and another without one
        Lead ldWithSlot = createLead('Lead with Slot');
        Lead ldNoSlot = createLead('Lead without Slot');
        
        Id queueId = [Select Id from Group where DeveloperName = :System.Label.LeadQueue and Type = 'Queue'].Id;
        
        ldWithSlot.OwnerId = queueId;
        ldNoSlot.OwnerId = queueId;
        ldNoSlot.Next_Activity_Tier__c = '';
        ldWithSlot.Next_Activity_Tier__c = '';
        
        List<Lead> newLeads = new List<Lead>{ldWithSlot, ldNoSlot};
        insert newLeads;
        
        Test.startTest();
            Apexpages.StandardSetController setcon = new ApexPages.StandardSetController(newLeads);
            LeadAssignmentByPriorityExt testExt = new LeadAssignmentByPriorityExt(setcon);
            testExt.redirect();
        Test.stopTest();
            
    }

     // Create a Lead
    private static Lead createLead(String lastName) {
        Lead ld = new Lead();
        ld.Company = lastName;
        ld.LastName = lastName;
        ld.Company = lastName;
        ld.Next_Activity_Tier__c = 'Tier 1';
        ld.Next_Activity_Due_Date__c = System.now().addDays(-1);
        return ld;
    }


    
}