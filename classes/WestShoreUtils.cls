/*
** Class:  WestShoreUtils
** Created by OpFocus on 10/26/2015
** Description: Common utility methods and wrapper classes for use with Scheduler, Confirmation & Staff Assignment Pages
*/  
global with sharing class WestShoreUtils {

    public static final String WINDOWS_AND_DOORS = 'Windows & Doors';
    public static final String SHOWERS_AND_BATHS = 'Showers & Baths';
    public static final String BOTH = WINDOWS_AND_DOORS + ' and ' + SHOWERS_AND_BATHS;
    public static final String AVAILABLE = 'Available';
    public static final String SETT = 'Set';
    public static final String CONFIRMED = 'Confirmed';
    public static final String SELECTED = 'Selected';
    public static final String STAFFED = 'Staffed';
    public static final String CANCELED = 'Canceled';
    public static final String RESCHEDULED = 'Rescheduled';
    public static final String ATTEMPTED_TO_CONTACT = 'Attempted to Contact';
    public static final String PTO = 'PTO';
    public static final String NONE = 'None';
    public static final String ALL = 'All';

    public static final Map<String, String> mapActionsByStatus = new Map<String, String>();

    // Holds results from Controller methods that save changes
    global with sharing class RemoteActionResults {
        public Boolean isSuccess {get; set;}
        public String  details {get; set;}

        public RemoteActionResults(Boolean blnIsSuccess, String details) {
            this.isSuccess = blnIsSuccess;
            this.details = details;
        }
    }

    public class AppointmentFilters {
        public String productGroup {get; set;}
        public String region {get; set;}
        public String residenceId {get; set;}
        public String apptTypes {get; set;}
        public String startDate {get; set;}
    }

    public abstract class EventWrapper {
        public Integer lastHour {get; set;}
        public String name {get; set;}
        public String selDate {get; set;}
        public Integer startTimeHour {get; set;}
        public Integer startTimeMinute {get; set;}
        public Integer endTimeHour {get; set;}
        public Integer endTimeMinute {get; set;}
        public Integer apptStartTimeHour {get; set;}
        public Integer apptStartTimeMinute {get; set;}        
        public Integer apptEndTimeHour {get; set;}
        public Integer apptEndTimeMinute {get; set;}        
        public Integer duration {get; set;}
        public Integer totalDuration {get; set;}
        public Integer buffer {get; set;}
        public Integer numQuarterHours {get; set;}
        public String status {get; set;}
        public String requestedStatus {get; set;}
        public String requestedStatusAction {get; set;}
        public String eventType {get; set;}
        public Id region {get; set;}
        public String regionName {get; set;}
        public String id {get; set;}
        public Id team {get; set;} 
        public String requestedTeam {get; set;}
        public String requestedTeamName {get; set;}
        public String addlTeam {get; set;}
        public String addlTeamName {get; set;}
        public Residence residence {get; set;}
        public String residenceId {get; set;} 
        public String notes {get; set;}
        public String type {get; set;}
        public Boolean isAvailable {get; set;}
        public String subType {get; set;}
        public String productGroup {get; set;}
        public String displayProductGroup {get; set;}
        public Boolean isAccount {get; set;}
        public String street {get; set;}
        public String city {get; set;}
        public String state {get; set;}
        public String postalCode {get; set;}
        public String phone {get; set;}
        public String resultReason {get; set;}
        public Boolean attemptedToContact {get; set;}
        public Boolean isOneLegger {get; set;}
        public Integer numPeople {get; set;}
        public Boolean needToBeHome {get; set;}
        public Boolean isPlaceHolder {get; set;}
        public String[] teamMembers {get; set;}
        public String nextDueDate {get; set;}
        public String nextOppStage {get; set;}

        // details specified when an appointment is being rescheduled
        public Boolean isBeingRescheduled {get; set;}
        public String rescheduledDate {get; set;}
        public String rescheduledTime {get; set;}
        public String rescheduledStatus {get; set;}

        public abstract Id getResidence();
        public abstract void setResidence(SObject acctOrLead);

        public String jsonData {
            get {
                JSONGenerator jsonGenerator = JSON.createGenerator(true);
                jsonGenerator.writeStartObject();

                jsonGenerator.writeObjectField('selDate', this.selDate);
                jsonGenerator.writeObjectField('duration', this.duration);
                jsonGenerator.writeObjectField('buffer', this.buffer);
                if (this.totalDuration != null) {
                    jsonGenerator.writeObjectField('totalDuration', this.totalDuration);
                }
                jsonGenerator.writeObjectField('name', this.name);
                jsonGenerator.writeObjectField('status', this.status);
                jsonGenerator.writeObjectField('requestedStatus', requestedStatus);
                jsonGenerator.writeObjectField('requestedStatusAction', requestedStatusAction);

                jsonGenerator.writeObjectField('isAvailable', this.status.equals(AVAILABLE));
                jsonGenerator.writeObjectField('type', this.type);
                if (this.subType != null) {   
                    jsonGenerator.writeObjectField('subType', this.subType);
                } else {
                    jsonGenerator.writeObjectField('subType', NONE);
                }
                jsonGenerator.writeObjectField('id', this.id);
                jsonGenerator.writeObjectField('startTime', startTime);
                jsonGenerator.writeObjectField('numQuarterHours', numQuarterHours);
                if (this.region != null) {
                    jsonGenerator.writeObjectField('region', this.region);
                    jsonGenerator.writeObjectField('regionName', this.regionName);                    
                }
                if (this.residence != null) {
                    jsonGenerator.writeBooleanField('isAccount', this.residence.isAccount);
                    jsonGenerator.writeObjectField('street', this.residence.street);
                    jsonGenerator.writeObjectField('city', this.residence.city);
                    jsonGenerator.writeObjectField('state', this.residence.state);
                    jsonGenerator.writeObjectField('postalCode', this.residence.postalCode);
                    if (this.residence.name != null) {
                        jsonGenerator.writeObjectField('residentName', this.residence.name);
                    }
                    if (this.residence.phone != null) {
                        jsonGenerator.writeObjectField('phone', this.residence.phone);                      
                    }
                    jsonGenerator.writeObjectField('residenceId', this.residence.id);
                }               
                if (notes != null) {
                    jsonGenerator.writeObjectField('notes', notes);
                }
                if (team != null) {
                    jsonGenerator.writeObjectField('team', team);
                }
                if (requestedTeam != null) {
                    jsonGenerator.writeObjectField('requestedTeam', requestedTeam);
                    jsonGenerator.writeObjectField('requestedTeamName', requestedTeamName);
                }
                if (addlTeam != null) {
                    jsonGenerator.writeObjectField('addlTeam', this.addlTeam);
                    jsonGenerator.writeObjectField('addlTeamName', this.addlTeamName);
                }

                jsonGenerator.writeObjectField('productGroup', this.productGroup);
                if (this.resultReason != null && this.resultReason.length() != 0) {
                    jsonGenerator.writeObjectField('resultReason', this.resultReason);
                }
                if (this.attemptedToContact != null) {
                    jsonGenerator.writeObjectField('attemptedToContact', this.attemptedToContact);
                }

                if (this.teamMembers != null) {
                    jsonGenerator.writeObjectField('teamMembers', String.join(this.teamMembers,','));
                }

                jsonGenerator.writeObjectField('isOneLegger', this.isOneLegger);
                jsonGenerator.writeObjectField('numPeople', this.numPeople);
                jsonGenerator.writeObjectField('needToBeHome', this.needToBeHome);

                if (this.nextDueDate != null) {
                    jsonGenerator.writeObjectField('nextDueDate', this.nextDueDate);
                }
                if (this.nextOppStage != null) {
                    jsonGenerator.writeObjectField('nextOppStage', this.nextOppStage);
                }

                jsonGenerator.writeEndObject();
                return jsonGenerator.getAsString();
            }
            set;
        }

        public String endTime {
            get {
                endTime = adjustTime(endTime, endTimeHour, endTimeMinute);
                return endTime;
            }
            set;
        }
        public String startTime {
            get {
                startTime = adjustTime(startTime, startTimeHour, startTimeMinute);
                return startTime;
            }
            set;
        }

        public String apptStartTime {
            get {
                apptStartTime = convertFromMIL(apptStartTime, apptStartTimeHour, apptStartTimeMinute);
                return apptStartTime;
            }
            set;
        }

        public String apptEndTime {
            get {
                apptEndTime = convertFromMIL(apptEndTime, apptEndTimeHour, apptEndTimeMinute);
                return apptEndTime;
            }
            set;
        }

        private String convertFromMIL(String value, Integer hour, Integer minute) {
            if (hour == null) {
                return '';
            }
            Boolean blnMorning = hour <= 12;

            value = (blnMorning ? hour : hour - 12) + ':' +  minute + (minute == 0 ? '0' : '');
            return value;
        }

        private void adjustTimesForBusinessDay(BaseApptCon con) {
            // get the last hour from Controller
            Integer firstHour = con.firstHour;
            Integer lastHour = con.lastHour;

            if (this.startTimeHour < firstHour && this.endTimeHour < firstHour) {
                // if this appointment starts befpre the first hour, 
                // truncate the start time to be the first hour
                this.startTimeHour = firstHour; 
                this.startTimeMinute= 0; 
                this.endTimeHour = firstHour;
                this.endTimeMinute = 15;
                // adjust numQuarterHours to be 1 (15 minutes)
                this.numQuarterHours = 1;
                this.startTime = adjustTime(this.startTime, this.startTimeHour, this.startTimeMinute);
                this.endTime = adjustTime(this.endTime, this.endTimeHour, this.endTimeMinute);
                return;
            }

            if (this.endTimeHour >= lastHour && this.startTimeHour >= lastHour) {
                // if this appointment starts befpre the first hour, 
                // truncate the start time to be the first hour
                this.startTimeHour = lastHour-1; 
                this.startTimeMinute= 45; 
                this.endTimeHour = lastHour;
                this.endTimeMinute = 0;
                // adjust numQuarterHours to be 1 (15 minutes)
                this.numQuarterHours = 1;
                this.startTime = adjustTime(this.startTime, this.startTimeHour, this.startTimeMinute);
                this.endTime = adjustTime(this.endTime, this.endTimeHour, this.endTimeMinute);

                return;
            }

            if (this.startTimeHour < firstHour) {
                // if this appointment starts befpre the first hour, 
                // truncate the start time to be the first hour
                this.startTimeHour = firstHour; 
                // also adjust numQuarterHours which dictates how many cells it takes up
                this.numQuarterHours = (endTimeHour - startTimeHour)*4;
                if (this.endTimeMinute != 0) {
                    this.numQuarterHours += this.endTimeMinute/15;
                }
                this.startTime = adjustTime(this.startTime, this.startTimeHour, this.startTimeMinute);
            }

            if (this.endTimeHour > lastHour || (this.endTimeHour == lastHour && this.endTimeMinute != 0)) {
                // if this appointment ends after the last hour, 
                // truncate the end time to be the last hour.
                this.endTimeHour = lastHour;
                this.endTimeMinute = 0;
                // also adjust numQuarterHours which dictates how many cells it takes up
                this.numQuarterHours = (endTimeHour - startTimeHour)*4;
                if (this.startTimeMinute != 0) {
                    // make sure we do not create a negative numQuarterHours value
                    this.numQuarterHours = (this.numQuarterHours - this.startTimeMinute/15 < 0 ? 1 : 
                        (this.numQuarterHours - this.startTimeMinute/15));
                }
                this.endTime = adjustTime(this.endTime, this.endTimeHour, this.endTimeMinute);
            }
        }
        private String adjustTime(String value, Integer hour, Integer minute) {
            if (hour == null) {
                if (value != null) {
                    return value;
                }
                return '';
            }
            value = hour + ':' + minute + (minute == 0 ? '0' : '');
            return value;
        }
    }

    public class AppointmentWrapper extends EventWrapper {
        public Appointment_Slot__c apptSlot {get; set;}

        // dummy constructor
        public AppointmentWrapper() {
            this.isPlaceHolder = true;
        }

        public AppointmentWrapper(BaseApptCon con, Appointment_Slot__c apptSlot) {
            this.isPlaceHolder = false;
            this.type = apptSlot.Type_Of_Appointment__c;
            this.apptSlot = apptSlot;
            this.eventType = 'Appointment';
            this.name = apptSlot.Name;
            this.status = apptSlot.Status__c;
            this.requestedStatus = apptSlot.Requested_Status__c == null ? (this.status.equals(AVAILABLE) ? SETT : 
                (this.status.equals(SETT) ? (this.type.equals(BaseApptCon.OPERATIONS) ? STAFFED : CONFIRMED) : 
                (this.status.equals(CONFIRMED) ? STAFFED : null))) : apptSlot.Requested_Status__c;
            this.requestedStatusAction = mapActionsByStatus.get(this.requestedStatus) == null ? NONE : 
                mapActionsByStatus.get(this.requestedStatus);
            this.team = apptSlot.Team__c;
            this.requestedTeam = apptSlot.Requested_Team__c;
            this.requestedTeamName = apptSlot.Requested_Team__c == null ? null : apptSlot.Requested_Team__r.Name;
            this.addlTeam = apptSlot.Additional_Team__c;
            this.addlTeamName = apptSlot.Requested_Team__c == null ? null : apptSlot.Additional_Team__r.Name;
            
            this.startTimeHour = apptSlot.Start_Time__c.hour();
            this.startTimeMinute = apptSlot.Start_Time__c.minute();
            this.apptStartTimeHour = apptSlot.Start_Time__c.hour();
            this.apptStartTimeMinute = apptSlot.Start_Time__c.minute();
            this.duration =  apptSlot.Duration__c == null ? 90 : Integer.valueOf(apptSlot.Duration__c);
            this.totalDuration = apptSlot.Total_Duration__c == null || apptSlot.Total_Duration__c == 0 ? 
                Integer.valueOf(apptSlot.Duration__c) : Integer.valueOf(apptSlot.Total_Duration__c);
            this.buffer = apptSlot.Standard_Buffer__c == 0 ? 90 : Integer.valueOf(apptSlot.Standard_Buffer__c);
            calculateQuarterHours();
            this.endTimeHour = apptSlot.Start_Time__c.addMinutes(this.totalDuration).hour();
            this.endTimeMinute = apptSlot.Start_Time__c.addMinutes(this.totalDuration).minute();
            this.apptEndTimeHour = apptSlot.Start_Time__c.addMinutes(this.duration).hour();
            this.apptEndTimeMinute = apptSlot.Start_Time__c.addMinutes(this.duration).minute();
            this.selDate = apptSlot.Date__c == null ? apptSlot.Start_Time__c.date().format() : apptSlot.Date__c.format();
            this.region = apptSlot.Region__c;
            this.regionName = apptSlot.Region__r.Name == null ? '' : apptSlot.Region__r.Name;
            this.productGroup = apptSlot.Product_Group__c == null ? '' : apptSlot.Product_Group__c;
            this.id = apptSlot.Id != null ? String.valueOf(apptSlot.Id) : 
                String.valueOf(team) + startTimeHour + startTimeMinute;
            this.notes = apptSlot.Notes__c;
            this.displayProductGroup = apptSlot.Short_Prod_Grp_Name__c == null ? '' : apptSlot.Short_Prod_Grp_Name__c;
            this.subType = apptSlot.Sub_Type__c;
            this.resultReason = apptSlot.Result_Reason__c == null ? '' : apptSlot.Result_Reason__c;
            this.attemptedToContact = apptSlot.Attempted_to_Contact__c;
            this.isOneLegger = apptSlot.One_Legger__c == null ? false : apptSlot.One_Legger__c;
            this.isBeingRescheduled = false;
            this.numPeople = apptSlot.Needed_for_Job__c == null ? 1 : Integer.valueOf(apptSlot.Needed_for_Job__c);
            this.needToBeHome = apptSlot.Need_To_Be_Home__c == null ? false : apptSlot.Need_To_Be_Home__c;
            adjustTimesForBusinessDay(con);
            getTeamMembers(apptSlot);
            this.nextDueDate = apptSlot.Set_Next_Due_Date__c == null ? '' : apptSlot.Set_Next_Due_Date__c.format();
            this.nextOppStage = apptSlot.Set_Opportunity_Stage__c;
        }

        private void getTeamMembers(Appointment_Slot__c appt) {
            List<String> lstTeamMemberIds = new List<String>();
            for (Team_Member__c teamMember : appt.Team_Members__r) {
                lstTeamMemberIds.add(teamMember.Contact__c);
            }
            this.teamMembers = lstTeamMemberIds;
        }

        private void calculateQuarterHours() {
            this.numQuarterHours =  (this.totalDuration == null ? 0 : (this.totalDuration / 15));

            // if our totalDuration is not divisible by 15 then adjust number of quarter hours
            Integer remainder = Math.mod(this.totalDuration, 15);
            if (remainder != 0) {
                Integer newOffset = 0;
                if (remainder > 7) {
                    this.numQuarterHours += 1;
                }
            }
        }

        public override Id getResidence() {
            if (residence != null) {
                return residence.id;
            }
            // if we have no appoinment, dont bother looking any further - just return null
            if (apptSlot == null) {
                return null;
            }

            return apptSlot.Account__c != null ? apptSlot.Account__c : 
                (apptSlot.Lead__c != null ? apptSlot.Lead__c : null);
        }

        public override void setResidence(SObject acctOrLead) {
            // set Residence Details        
            if (acctOrLead instanceof Account) {
                // dealing with an Account
                residence = new Residence((Account)acctOrLead);
            } else {
                // dealing with a Lead
                residence = new Residence((Lead)acctOrLead);
            }
            // reset region association & all attributes directly inherited from residence 
            // ONLY if they have a valid value, otherwise keep existing Region designation
            if (residence.region != null) {
                this.region = residence.region;
                this.regionName = residence.regionName;
            }
            this.city = residence.city;
            this.street = residence.street;
            this.state = residence.state;
            this.postalCode = residence.postalCode;
            this.phone = residence.phone;
            this.residenceId = acctOrLead.Id;
        }
    }

    public class PTOWrapper extends EventWrapper {
        public Event calEvent {get; set;}

        public PTOWrapper(BaseApptCon con, Event calEvent, Team__c team) {
            this.isPlaceHolder = false;
            this.calEvent = calEvent;
            this.eventType = calEvent.Type;
            this.startTimeHour = calEvent.StartDateTime.hour();
            this.startTimeMinute = calEvent.StartDateTime.minute();
            this.apptStartTimeHour = calEvent.StartDateTime.hour();
            this.apptStartTimeMinute = calEvent.StartDateTime.minute();
            this.duration = calEvent.DurationInMinutes;
            this.totalDuration = calEvent.DurationInMinutes;
            this.buffer = 0;
            this.numQuarterHours = (this.duration == null ? 0 : (this.duration / 15));
            this.endTimeHour = calEvent.EndDateTime.hour();
            this.endTimeMinute = calEvent.EndDateTime.minute();
            this.apptEndTimeHour = this.endTimeHour;
            this.apptEndTimeMinute = this.endTimeMinute;
            this.selDate = calEvent.ActivityDate.format();
            this.team = team.Id;
            this.region = team.Region__c;
            this.regionName = team.Region__r.Name == null ? '' : team.Region__r.Name;
            this.id = String.valueOf(calEvent.Id);
            this.name = calEvent.Subject;
            this.status = PTO;
            this.type = PTO;
            this.notes = calEvent.Description;
            adjustTimesForBusinessDay(con);
        }

        // There is no Residence associated with Calendar Events such as ours so we do nothing
        public override Id getResidence() {
            return null;
        }

        public override void setResidence(SObject acctOrLead) {
        }
    }

     // Values to populate Appointment Time select option components.  Defined in a custom setting
    public class WSSelectOption {
        public String label {get; set;}
        public String value {get; set;}

        public WSSelectOption(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

    // get the list of Possible Appointment Times for the given appointment type
    public static List<WSSelectOption> getTimeValues(List<String> apptTypes) {
        List<WSSelectOption> lstTimeValues = new List<WSSelectOption>();

        for (Appt_Time_Values__c timeValue : [select Label__c, Value__c 
                from Appt_Time_Values__c
                where Appt_Type__c in :apptTypes or Appt_Type__c=:ALL
                order by Name]) {
            lstTimeValues.add(new WSSelectOption(timeValue.Label__c, timeValue.Value__c));
        }
        return lstTimeValues;
    }

    // get the list of Duration Values for the given appointment type
    public static List<WSSelectOption> getDurationValues(List<String> apptTypes) {
        List<WSSelectOption> lstDurationValues = new List<WSSelectOption>();

        for (Appt_Duration_Values__c duration : [select Label__c, Value__c 
                from Appt_Duration_Values__c
                where Appt_Type__c in :apptTypes OR Appt_Type__c=:ALL
                order by Name]) {
            lstDurationValues.add(new WSSelectOption(duration.Label__c, duration.Value__c));
        }
        return lstDurationValues;
    }

    public static List<WSSelectOption> getResultReasons() {
        List<WSSelectOption> lstResultReasons = new List<WSSelectOption>();
        // get list of picklist values from Appointment Slot metadata
        Schema.SObjectType objType = Appointment_Slot__c.getSObjectType();
        Schema.DescribeSObjectResult objDesc = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDesc.fields.getMap();

        lstResultReasons.add(new WSSelectOption(NONE, NONE));
        List<Schema.PicklistEntry> plValues =
            fieldMap.get('Result_Reason__c').getDescribe().getPickListValues();
        for (Schema.PicklistEntry plValue : plValues) {
            // rather than using the label specified by picklist, we will use
            // the abbreviated version to save on real estate when displaying appointments.
            lstResultReasons.add(new WSSelectOption(plValue.getLabel(), plValue.getValue()));
        }
        return lstResultReasons;
    }

    // get the list of Product Group Values 
    public static List<WSSelectOption> getProductGroups(List<Prod_Grp_Mappings__c> prodGrpMappings) {
        List<WSSelectOption> lstProductGroups = new List<WSSelectOption>();
        // get list of picklist values from Appointment Slot metadata
        Schema.SObjectType objType = Appointment_Slot__c.getSObjectType();
        Schema.DescribeSObjectResult objDesc = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDesc.fields.getMap();

        // first add All value to PickList
        lstProductGroups.add(new WSSelectOption(ALL, ALL));

        List<Schema.PicklistEntry> plValues =
            fieldMap.get('Product_Group__c').getDescribe().getPickListValues();
        for (Schema.PicklistEntry prdGrpValue : plValues) {
            // rather than using the label specified by picklist, we will use
            // the abbreviated version to save on real estate when displaying appointments.
            lstProductGroups.add(new WSSelectOption(prdGrpValue.getLabel(), prdGrpValue.getValue()));
        }
        return lstProductGroups;
    }

    // get the list of Regions  
    public static List<WSSelectOption> getRegions() {
        List<WSSelectOption> lstRegions = new List<WSSelectOption>();

        lstRegions.add(new WSSelectOption(ALL, ALL));
        for (Region__c region : [select Id, Name from Region__c order by Name]) {
            lstRegions.add(new WSSelectOption(region.Name, region.Id));
        }
        return lstRegions;
    }

    public static List<WSSelectOption> getSubTypes(List<String> apptTypes) {
        List<WSSelectOption> lstSubTypes = new List<WSSelectOption>();

        // first add All value to PickList
        for (Sub_Type__c subType : [select Id, Label__c, Value__c
                from Sub_Type__c where Appt_Type__c in :apptTypes or Appt_Type__c = :ALL
                order by Name]) {
            lstSubTypes.add(new WSSelectOption(subType.Label__c, subType.Value__c));
        }
        return lstSubTypes;
    }

    public static List<WSSelectOption> getStatuses(List<String> apptTypes) {
        List<WSSelectOption> lstStatuses = new List<WSSelectOption>();

        for (Status__c status : [select Id, Label__c, Value__c, Pending_Value__c 
                from Status__c where Appt_Type__c in :apptTypes or Appt_Type__c = :ALL 
                order by Name]) {
            mapActionsByStatus.put(status.Value__c, status.Pending_Value__c);
            lstStatuses.add(new WSSelectOption(status.Label__c, status.Value__c));
        }
        return lstStatuses;
    }

    public static List<WSSelectOption> getApptTypeFilters() {
        List<WSSelectOption> lstApptTypes = new List<WSSelectOption>();
        lstApptTypes.add(new WSSelectOption(ALL, BaseApptCon.PREINSTALL + ',' + BaseApptCon.INSTALL 
            + ',' + BaseApptCon.SERVICE));
        lstApptTypes.add(new WSSelectOption(BaseApptCon.PREINSTALL, BaseApptCon.PREINSTALL));
        lstApptTypes.add(new WSSelectOption(BaseApptCon.INSTALL + ' and ' + BaseApptCon.SERVICE, 
            BaseApptCon.INSTALL + ',' + BaseApptCon.SERVICE));

        return lstApptTypes;
    }

    public static List<WSSelectOption> getTeamMembers() {
        List<WSSelectOption> lstTeamMembers = new List<WSSelectOption>();
        Id westShoreAcctId = [select Id from Account where Name='West Shore, Inc' limit 1].Id;

        // get all contacts that are employees of West Shore and who handle Operations appointments
        List<Contact> lstContacts = [select Id, Name, Appointment_Types__c 
            from Contact where AccountId=:westShoreAcctId and Appointment_Types__c != null];
        // TODO : Replace List with Map & Dynamically change what is shown in Picklist 
        // based on sub type of appointment selected by user in dialog box
        Map<String, List<WSSelectOption>> mapContactsByApptType = new Map<String, List<WSSelectOption>>();
        for (Contact contact : lstContacts) {
            // Appointment_Types__c is a multi-select picklist so break it up into individual values
            // as we have an entry in our map for each appointment type
            String[] apptTypes = contact.Appointment_Types__c.split(';');
            for (String apptType : apptTypes) {
                if (mapContactsByApptType.get(apptType) == null) {
                    mapContactsByApptType.put(apptType, new List<WSSelectOption>());
                }
                mapContactsByApptType.get(apptType).add(new WSSelectOption(contact.Name, contact.Id));                
            }
            lstTeamMembers.add(new WSSelectOption(contact.Name, contact.Id));
        }
        return lstTeamMembers;
    }

    public static List<WSSelectOption> getNextOppStages() {
        List<WSSelectOption> lstNextOppStages = new List<WSSelectOption>();
        // get list of picklist values from Appointment Slot metadata
        Schema.SObjectType objType = Appointment_Slot__c.getSObjectType();
        Schema.DescribeSObjectResult objDesc = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDesc.fields.getMap();

        // first add a default value of None
        lstNextOppStages.add(new WSSelectOption('None', 'None'));
        List<Schema.PicklistEntry> plValues =
            fieldMap.get('Set_Opportunity_Stage__c').getDescribe().getPickListValues();
        for (Schema.PicklistEntry plValue : plValues) {
            lstNextOppStages.add(new WSSelectOption(plValue.getLabel(), plValue.getValue()));
        }
        return lstNextOppStages;
    }
}