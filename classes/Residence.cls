public with sharing class Residence {
    // Represents the Location where appointment is being held. 
    // It is either an Account of record type Residence or a Lead (for new customers)
    public String name {get; set;}
    public String phone {get; set;}
    public String street {get; set;}
    public String city {get; set;}
    public String state {get; set;}
    public String postalCode {get; set;}
    public Id region {get; set;}
    public String regionName {get; set;}
    public Account acct {get; set;}
    public Lead lead {get; set;}
    public Boolean isAccount {get; set;}
    public Id id {get; set;}

    public Residence(Lead lead) {
        this.lead = lead;
        this.isAccount = false;
        this.street = lead.Street == null ? '' : lead.Street;
        this.city = lead.City == null ? '' : lead.City;
        this.state = lead.State == null ? '' : lead.State;
        this.postalCode = lead.PostalCode == null ? '' : lead.PostalCode;
        this.name = lead.name;
        this.phone = lead.phone;
        this.id = lead.Id;
        this.region = lead.Region__c;
        this.regionName = lead.Region__r.Name == null ? '' : lead.Region__r.Name;
    }

    public Residence(Account acct) {
        this.acct = acct;
        this.isAccount = true;
        this.street = acct.BillingStreet == null ? '' : acct.BillingStreet;
        this.city = acct.BillingCity == null ? '' : acct.BillingCity;
        this.state = acct.BillingState == null ? '' : acct.BillingState;
        this.postalCode = acct.BillingPostalCode == null ? '' : acct.BillingPostalCode;
        this.id = acct.Id;
        this.region = acct.Region__c;
        this.regionName = acct.Region__r.Name == null ? '' : acct.Region__r.Name;

        // if we have a Primary Contact, then pull name & phone from that record
        if (acct.Contacts != null && acct.Contacts.size() != 0) {
            this.name = acct.Contacts.get(0).Name;
            this.phone = acct.Contacts.get(0).Phone;
        }
    }
}