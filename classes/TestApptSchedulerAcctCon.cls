/*
** Class:  TestApptSchedulerAcctCon
** Created by OpFocus on 1/14/2016
** Description: Unit test for Appointment Scheduler Controller for Account
*/  
@isTest
private class TestApptSchedulerAcctCon {

    private static final Boolean testInit = true;
    private static final Boolean testSelectAndSetApptForAcct = true;
    private static final Boolean testSelectApptWithAddlTeam = true;
    private static final Boolean testChangeDate = true;
    private static final Boolean testConflictingAppts = true;
    private static final Boolean testApplyFilters = true;
    private static final Boolean testApplyingFilters = true;
    private static final Boolean testRemoveAppts = true;
    private static final Boolean testBuildUrlParam = true;

    @isTest
    static void testInit() {
        System.assert(testInit, 'Test Disabled.');
        TestApptHelper.initData(false, TestApptHelper.ACCT_SCHEDULER, Page.ApptSchedulerAcct);

        Test.setCurrentPageReference(Page.ApptSchedulerAcct);
        ApexPages.currentPage().getParameters().put('id', TestApptHelper.lead.Id);

        Test.startTest();
            TestApptHelper.con = new ApptSchedulerCon();
        Test.stopTest();

        // verify that the correct number of appointment slots were added to controller
        System.assertEquals(5, TestApptHelper.con.apptRows.size(), 
            'Should have been 1 row created for each team tember and there are 5 teams.');

        // as there are no conflicting events, each team member should have a single row
        System.assertEquals(1, 
            TestApptHelper.con.apptRows.get(TestApptHelper.mapTeamsByType.get('Sales').Id).size(), 
            'Incorrect number of Appointment Rows created for Sales Team.');
    }

    @isTest
    static void testChangeDate() {
        System.assert(testChangeDate, 'Test Disabled.');
        TestApptHelper.initData(false, TestApptHelper.ACCT_SCHEDULER, Page.ApptSchedulerAcct);

        TestApptHelper.testChangeDate(TestApptHelper.ACCT_SCHEDULER, Page.ApptSchedulerAcct, 'Sales');
    }

    @isTest
    static void testSelectAndSetApptForAcct() {
        System.assert(testSelectAndSetApptForAcct, 'Test Disabled.');
        TestApptHelper.initData(true, TestApptHelper.ACCT_SCHEDULER, Page.ApptSchedulerAcct);
        TestApptHelper.testSelectAppt(TestApptHelper.ACCT_SCHEDULER, Page.ApptSchedulerAcct, 
        	WestShoreUtils.SETT, 'Setting');
    }

    @isTest
    static void testApplyFilters() {
        System.assert(testApplyFilters, 'Test Disabled.');
        TestApptHelper.initData(true, TestApptHelper.ACCT_SCHEDULER, Page.ApptSchedulerAcct);
        TestApptHelper.testApplyFilters(TestApptHelper.ACCT_SCHEDULER, Page.ApptSchedulerAcct);
    }

    @isTest
    static void testApplyingFilters() {
        System.assert(testApplyingFilters, 'Test Disabled.');
        TestApptHelper.initData(false, TestApptHelper.ACCT_SCHEDULER, Page.ApptSchedulerAcct);

        String prodGrp = 'Windows & Doors';

        Date myDate = Date.today();
        Region__c london = new Region__c(Name='London, England');
        Region__c paris = new Region__c(Name='Paris, France');
        Region__c florence = new Region__c(Name='Florence, Italy');
        insert new List<Region__c> {london, paris, florence};
        
        // now lets' update our Team to have the same Product Group & Region values
        Team__c ourteam = TestApptHelper.mapTeamsByType.get('Sales');
        ourTeam.Product_Group__c = 'Showers & Baths';
        ourTeam.Region__c = florence.Id;
        update ourTeam;

        // now construct controller with filter values
        PageReference testPage = Page.ApptSchedulerAcct;
        Test.setCurrentPageReference(testPage);
        ApexPages.currentPage().getParameters().put('prodGrp', prodGrp);
        ApexPages.currentPage().getParameters().put('region', london.Id);
        ApexPages.currentPage().getParameters().put('id', TestApptHelper.acct.Id);
        Test.startTest();
            TestApptHelper.con = new ApptSchedulerCon();
        Test.stopTest();

        // there should only be 0 TeamAppointmentRow entries in our map as none of 
        // our teams are in London region nor do they service our prodGrp
        System.assertEquals(0, TestApptHelper.con.apptRows.size(), 
            'Failed to filter teams by product group & region.');
    }
}