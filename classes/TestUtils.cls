public with sharing class TestUtils {
	public static User createUser(String userName, String firstName, String lastName) {
		Profile prof = [Select Id From Profile Where Name='Standard User' limit 1];
		User newUser = new User();
		newUser.Email = userName+'@opfocus.com';
		newUser.Username = userName+'@opfocus.com';
		newUser.FirstName = firstName;
		newUser.LastName = lastName;
		newUser.Alias = userName;
		newUser.CommunityNickname = userName;
		newUser.TimeZoneSidKey = 'GMT';
		newUser.LocaleSidKey = 'en_US';
		newUser.EmailEncodingKey = 'ISO-8859-1';
		newUser.LanguageLocaleKey = 'en_US';
		newUser.ProfileId = prof.Id;
		return newUser;
	}

	public static Account createAccount(String name) {
		return new Account(Name=name, 
			RecordTypeId=[select Id from RecordType where SObjectType='Account' and DeveloperName='Residence' limit 1].Id);
	}

	public static Account createVendor(String name) {
		return new Account(Name=name, 
			RecordTypeId=[select Id from RecordType where 
				SObjectType='Account' and DeveloperName='Vendor' limit 1].Id);
	}


	public static Contact createContact(Account acct, String fName, String lName, String phone, 
		String street, String city, String state, String postalCode) {
		return new Contact(Account=acct, FirstName=fName, LastName=lName, Phone=phone,
			MailingStreet=street, MailingCity=city, MailingState=state, MailingPostalCode=postalCode,
			Appointment_Types__c='Install;Pre-Install;Service');
	}

	public static Lead createLead(String fName, String lName, String companyName) {
		return new Lead(FirstName=fName, LastName=lName, Company=companyName);
	}
}