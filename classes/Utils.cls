/*
** Class:  Utils
** Created On:  02/08/2016
** Created by:  OpFocus Team (bfinn)
** Description:  Helper class containing common utillity methods and helper classes
**/ 
public with sharing class Utils {

	// Returns a string containing a comma-separate list of field names suitable for
    // the SELECT clause of a SOQL query
    public static String getSelectFieldsFor(String id) {
        List<String> lstFieldNames = new List<String>();
        Map<String, Schema.SObjectField> mapFields = getMapFieldsByNameForObject(id);

        for (String fldSimpleName : mapFields.keyset()) {
            Schema.SObjectField fldToken = mapFields.get(fldSimpleName);
            Schema.DescribeFieldResult fldResult = fldToken.getDescribe();
            lstFieldNames.add(fldResult.getName());
        }
        lstFieldNames.sort();
        return String.join(lstFieldNames, ', ');
    }


    // Returns a map of field tokens for the SObject with the dgiven Id or Id prefix
    public static Map<String, Schema.SObjectField> getMapFieldsByNameForObject(String id) {
        String idPrefix = id.substring(0, 3);

        // If this map is already in our cache, just return it
        if (mapCachedFieldTokenMaps.containsKey(idPrefix)) {
            return mapCachedFieldTokenMaps.get(idPrefix);
        }

        // Get the map and add it to our cache
        Map<String, Schema.SObjectType> globalDescribeResults = Schema.getGlobalDescribe();
        for (String soName : globalDescribeResults.keyset()) {
            Schema.SObjectType soType = globalDescribeResults.get(soName);
            Schema.DescribeSObjectResult describeResult = soType.getDescribe();
            String soPrefix = describeResult.getKeyPrefix();
            if (soPrefix != null && soPrefix.equals(idPrefix)) {
                Map<String, Schema.SObjectField> mapFieldTokens = describeResult.fields.getMap();
                mapCachedFieldTokenMaps.put(idPrefix, mapFieldTokens);
                return mapFieldTokens;
            }
        }
        return null;
    }

    // A cache of field token maps, indexed by Id Prefix
    private static Map<String, Map<String, Schema.SObjectField>> mapCachedFieldTokenMaps =
        new Map<String, Map<String, Schema.SObjectField>>();
}