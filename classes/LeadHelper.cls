/* Class:       LeadHelper
 * Created On:  01/06/2016
 * Created by:  OpFocus Team
 * Description: Helper class for Lead object. For use with the Lead triggers
 */
public class LeadHelper {

    // When a Lead is converted to an Opportunity, update any Appointment_Slot__c records that refer to the Lead to 
    // make them also refer to the new Opportunity.
    public static void updateAppointmentSlots(Map<Id, Lead> newMap, Map<Id, Lead> oldMap) {

        // Find all Leads for which an OpportunityId was just defined
        Set<Id> setLeadIds = new Set<Id>();
        for (Lead newLead : newMap.values()) {
            Lead oldLead = oldMap.get(newLead.Id);
            if (newLead.IsConverted && !oldLead.IsConverted &&
                newLead.ConvertedOpportunityId != null && oldLead.ConvertedOpportunityId == null) {
                setLeadIds.add(newLead.Id) ;
            }
        }
        if (!setLeadIds.isEmpty()) {
            // Find all Appointment_Slot__c records for these Leads
            List<Appointment_Slot__c> lstApptSlots =
                [select id, Lead__c from Appointment_Slot__c where Lead__c in :setLeadIds];
            if (!lstApptSlots.isEmpty()) {
                for (Appointment_Slot__c slot : lstApptSlots) {
                    // Find the Lead for this slot, then update the slot to refer to the newly created Opp
                    Lead ld = newMap.get(slot.Lead__c);
                    if (ld != null) {
                        slot.Opportunity__c = ld.ConvertedOpportunityId;
                        slot.Account__c = ld.ConvertedAccountId;
                        slot.Contact__c = ld.ConvertedContactId;
                        slot.Lead__c = null;
                    }
                }
                update lstApptSlots;
            }
        }
    }
}