/*
** Class:  OperApptConfirmationCon
** Created by OpFocus on 1/12/2016
** Description: Controller for the Operations Appointment Confirmation & Staffing Page
**  @See OperApptConfirmation VF page
*/  
global with sharing class OperApptConfirmationCon extends BaseApptCon {

    static {
        BaseApptCon.myPage = Page.OperApptConfirmation;
    }

    global String apptTypeFiltersJSON {get; set;}
    global String teamMembersJSON {get; set;}

    private Map<String, String> mapTeamTypeToSubType = new Map<String, String>();
    
    global OperApptConfirmationCon() {
        apptType = OPERATIONS;
        lstApptTypes = new List<String>();
        lstApptTypes.add(PREINSTALL);
        lstApptTypes.add(INSTALL);
        lstApptTypes.add(SERVICE);
        selApptTypes = '';
        for (String apptType : lstApptTypes) {
            selApptTypes += apptType + ',';
        }
        selApptTypes = selApptTypes.substringBeforeLast(',');

        initialize();
        // reset our list of sub types
        apptTypeFiltersJSON = JSON.serialize(WestShoreUtils.getApptTypeFilters());
        subTypesJSON = JSON.serialize(WestShoreUtils.getSubTypes(new List<String>{OPERATIONS}));
        teamMembersJSON = JSON.serialize(WestShoreUtils.getTeamMembers());
        createMapTypeToSubType();
    }


    // Build query string for our Teams. Default query string will get all Teams for 
    // a single appointment type that that meet the following criteria:
    // 
    // 1. Services our type of appointment as specified by URL parameter: apptTypes
    //    AND handle appointments for selected Product Groups. If none or All selected, then this is omitted
    //    AND service the selected Region. If no Region selected or All, then this is omitted
    // OR 
    // 2. Team is for Set and Unassigned Appointments
    public override String buildQueryString() { 
        String apptTypes = '(';
        for (String apptType : selApptTypes.split(',')) {
            apptTypes += '\'' + apptType + '\',';
        }
        apptTypes += '\'' + SET_AND_UNASSIGNED + '\')';

        System.debug('=========> apptTypes = ' + apptTypes);
        return 'select Id, Name, Region__r.Name, Type__c, User__c from Team__c  ' + 
            'where Is_Additional__c = false ' + 
            'and ((Type__c in '  + apptTypes + ' '  +
            (selProdGrp.equals(ALL) ? '' : 'and Product_Group__c includes (\'' + selProdGrp + '\') ') +
            (selRegion.equals(ALL) ? '' : 'and Region__c = \'' + selRegion + '\' ') + ') ' +
            ') order by Display_Name__c';
    }

    // create an Available Appointment for calendar - additionally setting Sub Type so overriding base impl
    public override Appointment_Slot__c createAvailableAppt(Team__c team, Integer hour) {
        DateTime apptStartHour = DateTime.newInstance(ourDay.year(), ourDay.month(), ourDay.day(), hour, 0, 0);
        return new Appointment_Slot__c (
            Status__c = WestShoreUtils.AVAILABLE, Type_of_Appointment__c = apptType,
            Sub_Type__c = mapTeamTypeToSubType.get(team.Type__c), Duration__c = 60, Team__c = team.Id, 
            Standard_Buffer__c = 90, Name=String.join(lstApptTypes,',') + ' Appointment ' + ourDay.format(), 
            Start_Time__c = apptStartHour);
    }

    private void createMapTypeToSubType() {
        mapTeamTypeToSubType = new Map<String, String>();
        for (Sub_Type__c subType : [select Value__c, Team_Type__c 
                from Sub_Type__c where Appt_Type__c=:OPERATIONS]) {
            mapTeamTypeToSubType.put(subType.Team_Type__c, subType.Value__c);
        }
    }

    @RemoteAction
    global static  WestShoreUtils.RemoteActionResults changeDate(String jsonApptFilters, String jsonSelApptWrappers) {
        return BaseApptCon.applyFilters(jsonApptFilters, jsonSelApptWrappers, true);
    }

    // apply filters so we only see Design Consultants who handle selected Product Group(s) in selected Region(s)
    @RemoteAction
    global static WestShoreUtils.RemoteActionResults applyFilters(String jsonApptFilters, String jsonSelApptWrappers) {
        return BaseApptCon.applyFilters(jsonApptFilters, jsonSelApptWrappers, false);
    }

    @RemoteAction
    global static WestShoreUtils.RemoteActionResults selectAppt(String jsonSelApptWrappers, String jsonApptSlot,
           String jsonApptFilters) {
        return BaseApptCon.selectAppt(jsonSelApptWrappers, jsonApptSlot, 
            jsonApptFilters, false);
    }

    @RemoteAction
    global static WestShoreUtils.RemoteActionResults updateAppts(String jsonApptFilters, String jsonSelApptWrappers) {
        return BaseApptCon.updateAppts(jsonApptFilters, jsonSelApptWrappers);
    }
}