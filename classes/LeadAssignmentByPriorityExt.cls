/* Class:       LeadAssignmentByPriorityExt
 * Created On:  01/21/2016
 * Created by:  OpFocus Team
 * Description: Extension class that will get the next lead from queue and assigns to current user.
 */
public class LeadAssignmentByPriorityExt {
	
    private Lead leadFromQueue;
    private boolean isError;
    
    // Constructor for List View
    public LeadAssignmentByPriorityExt(Apexpages.StandardSetController setCon) {
        init();
    }    

    // Constructor
    public LeadAssignmentByPriorityExt(Apexpages.StandardController con) {
        init();            
    }

    // Init method called from Constructors
    private void init() {
        
        isError = false;
        
        try {
            // Get the GMT Time for the 
            String nowString = System.now().formatGMT('yyyy-MM-dd HH:mm:ss');
            nowString = nowString.replace(' ', 'T') + 'z';

            Id queueId = [Select Id from Group where DeveloperName = :System.Label.LeadQueue and Type = 'Queue'].Id;
            User currentUserInfo = [Select Id, Assignment_Tiers__c from User where Id = :UserInfo.getUserId()];
            String whereClause = '';
            for (String activityTier : currentUserInfo.Assignment_Tiers__c.split(';')) {
                whereClause += ' Next_Activity_Tier__c  = \'' + activityTier + '\'' + ' OR';  
            }
            whereClause = whereClause.removeEnd(' OR');
            whereClause = ' WHERE OwnerId = :queueId' + ' AND ' + '(' + whereClause + ')';
            whereClause += ' AND Next_Activity_Due_Date__c  <= ' + nowString;
            List<Lead> leadsInQueue = Database.query('SELECT Id from Lead ' + whereClause + ' order by Priority__c DESC LIMIT 1');
            
            if (leadsInQueue != null && leadsInQueue.size() > 0) {
                leadFromQueue = leadsInQueue[0];
            } else {
                isError = true;
            }
        } catch (Exception e) {
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, e.getMessage()));
        }
    }
    
    public Pagereference redirect() {
        if (!isError) {
            leadFromQueue = new Lead(OwnerId = UserInfo.getUserId(),Id=leadFromQueue.Id);
            update leadFromQueue;
          	return new Pagereference('/'+leadFromQueue.Id);          
        } else {
           	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, 'No Leads in the Queue'));
        	return null;
        }
    }
}