/*
** Class:  TestOperApptConfirmationCon
** Created by OpFocus on 1/14/2016
** Description: Unit test for Operations Appt Controller
*/ 	
@isTest
private class TestOperApptConfirmationCon {
	private static final Boolean testInit = true;
	private static final Boolean testConfirmAppt = true;
	private static final Boolean testApplyFilters = true;
	private static final Boolean testSelectAppt = true;
	private static final Boolean testChangeDate = true;
	private static final Boolean testAddTeamMembers = true;
	private static final Boolean testSetRequestedTeamForSet = true;

	@isTest
	static void testInit() {
		System.assert(testInit, 'Test Disabled.');
		TestApptHelper.initData(false, TestApptHelper.OPER_CONFIRMATION, Page.OperApptConfirmation);

	    PageReference testPage = Page.OperApptConfirmation;
	    Test.setCurrentPageReference(testPage);

		Test.startTest();
		    TestApptHelper.con = new ApptConfirmationCon();
		Test.stopTest();

		// verify that all our appointments were created in a single row
		List<BaseApptCon.TeamAppointmentRow> teamRows = TestApptHelper.con.apptRows.get(TestApptHelper.mapTeamsByType.get('Sales').Id);
		System.assertEquals(1, teamRows.size(), 'Did not create expected number of rows' );
	}


	@isTest
	static void testConfirmAppt() {
		System.assert(testConfirmAppt, 'Test Disabled.');
		TestApptHelper.initData(true, TestApptHelper.OPER_CONFIRMATION, Page.OperApptConfirmation);

		WestShoreUtils.AppointmentFilters filters = new WestShoreUtils.AppointmentFilters();
		filters.productGroup = 'All';
		filters.region = 'All';
    	filters.apptTypes = 'Install';
    	filters.startDate = Date.today().format();

		WestShoreUtils.AppointmentWrapper apptDetails = new WestShoreUtils.AppointmentWrapper();
		List<BaseApptCon.TeamAppointmentRow> teamRows = 
			TestApptHelper.con.apptRows.get(TestApptHelper.mapTeamsByType.get('Install').Id);

		WestShoreUtils.EventWrapper wrapper = teamRows.get(0).appts.get(0);
		apptDetails.id = ((WestShoreUtils.AppointmentWrapper)wrapper).apptSlot.Id;
		apptDetails.selDate = Date.today().format();
		apptDetails.startTime = '9:30';
		apptDetails.duration = 120;
		apptDetails.notes = 'test notes';
		apptDetails.residenceId = String.valueOf(TestApptHelper.lead.Id);
		apptDetails.status = 'Available';
		apptDetails.type = wrapper.type;
		apptDetails.subType = wrapper.subType;
		apptDetails.team = wrapper.team;
		apptDetails.addlTeam = TestApptHelper.mapTeamsByType.get('Service').Id;
		apptDetails.addlTeamName = 'Service Team';
		apptDetails.buffer = 90;
		apptDetails.requestedStatus = WestShoreUtils.CONFIRMED;
		apptDetails.requestedStatusAction = 'Confirming';
		apptDetails.productGroup = 'Windows & Doors';
		apptDetails.attemptedToContact = false;
		apptDetails.numQuarterHours = 14;
		apptDetails.name='name';

		BaseApptCon.SelectedApptWrapper selWrapper = new BaseApptCon.SelectedApptWrapper(apptDetails.Id, 
			apptDetails.requestedStatus, TestApptHelper.mapTeamsByType.get('Install').Id, 'Operations',
			TestApptHelper.mechanicsburg.Id);

		WestShoreUtils.RemoteActionResults results = null;

		Test.startTest();
			results = ApptConfirmationCon.updateAppts(JSON.serialize(filters),
				JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>{selWrapper}));
		Test.stopTest();

		System.assertEquals(true, results.isSuccess, 
			'Did not confirm Appointment correctly as expected. Error Details = ' + results.details);
	}
	
	@isTest
	static void testApplyFilters() {
		System.assert(testApplyFilters, 'Test Disabled.');
		TestApptHelper.initData(true, TestApptHelper.OPER_CONFIRMATION, Page.OperApptConfirmation);
		TestApptHelper.testApplyFilters(TestApptHelper.OPER_CONFIRMATION, Page.OperApptConfirmation);
	}

	@isTest
	static void testChangeDate() {
		System.assert(testChangeDate, 'Test Disabled.');
		TestApptHelper.initData(false, TestApptHelper.OPER_CONFIRMATION, Page.OperApptConfirmation);
		TestApptHelper.testChangeDate(TestApptHelper.OPER_CONFIRMATION, Page.OperApptConfirmation, 'Install');
	}

	@isTest
	static void testSelectAppt() {
		System.assert(testSelectAppt, 'Test Disabled.');
		TestApptHelper.initData(true, TestApptHelper.OPER_CONFIRMATION, Page.OperApptConfirmation);
		TestApptHelper.testSelectAppt(TestApptHelper.OPER_CONFIRMATION, Page.OperApptConfirmation, 
			WestShoreUtils.CONFIRMED, 'Confirming', 'Install', 'Operations');
	}

	@isTest 
	static void testSetRequestedTeamForSet() {
		System.assert(testSetRequestedTeamForSet, 'Test Disabled.');
		TestApptHelper.initData(true, TestApptHelper.OPER_CONFIRMATION, Page.OperApptConfirmation);
		TestApptHelper.testSelectAppt(TestApptHelper.OPER_CONFIRMATION, Page.OperApptConfirmation, 
			WestShoreUtils.SETT, 'Setting', 'Install', 'Operations');		
	}

	@isTest
	static void testAddTeamMembers() {
		System.assert(testAddTeamMembers, 'Test Disabled.');
		TestApptHelper.initData(true, TestApptHelper.OPER_CONFIRMATION, Page.OperApptConfirmation);

		// create and add some Team Members to our selected appointment
        WestShoreUtils.AppointmentWrapper apptDetails = new WestShoreUtils.AppointmentWrapper();
        WestShoreUtils.EventWrapper wrapper = 
            TestApptHelper.con.apptRows.get(TestApptHelper.mapTeamsByType.get('Install').Id).get(0).appts.get(0);
        apptDetails.id = ((WestShoreUtils.AppointmentWrapper)wrapper).Id;
        apptDetails.selDate = Date.today().format();
        apptDetails.startTime = '9:30';
        apptDetails.duration = 120;
        apptDetails.numQuarterHours = 14;
        apptDetails.residenceId = TestApptHelper.lead.Id;
        apptDetails.status = 'Available';
        apptDetails.notes = 'test notes';
        apptDetails.type = wrapper.type;
        apptDetails.subType = wrapper.subType;
        apptDetails.team = wrapper.team;
        apptDetails.buffer = 90;
        apptDetails.requestedStatus = 'Staffed';
        apptDetails.requestedStatusAction = 'Staff';
        apptDetails.productGroup = 'Windows & Doors';
        apptDetails.isOneLegger = false;
        apptDetails.numPeople = 1;
        apptDetails.name = 'name';
        apptDetails.needToBeHome = false;
        apptDetails.teamMembers = new String[] {TestApptHelper.lstContacts.get(0).Id, 
        	TestApptHelper.lstContacts.get(0).Id};
        apptDetails.region = TestApptHelper.mechanicsburg.Id;
        apptDetails.regionName = TestApptHelper.mechanicsburg.Name;
        
        WestShoreUtils.AppointmentFilters filters = new WestShoreUtils.AppointmentFilters();
        filters.productGroup = 'All';
        filters.region = 'All';
        filters.residenceId = TestApptHelper.lead.Id;
        filters.apptTypes = 'Install';
        filters.startDate = Date.today().format();

        WestShoreUtils.RemoteActionResults results = null;
        List<BaseApptCon.SelectedApptWrapper> lstWrappers = new List<BaseApptCon.SelectedApptWrapper>();
        Test.startTest();
            results = OperApptConfirmationCon.selectAppt(JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()), 
                JSON.serialize(apptDetails), 
                JSON.serialize(filters));
            System.assertEquals(true, results.isSuccess, 
                'Did not select Appointment correctly as expected. Error Details = ' + results.details);
        Test.stopTest();
	}
}