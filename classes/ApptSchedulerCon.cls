/*
** Class:  ApptSchedulerCon
** Created by OpFocus on 10/21/2015
** Description: Controller for the Appointment Scheduler VF Page
**  @See AppointmentScheduler VF page
*/  
global virtual with sharing class ApptSchedulerCon extends BaseApptCon {

    static {
        BaseApptCon.myPage = Page.ApptScheduler;
    }
    global Residence residence {get; set;}
    global String residenceId {get; set;}
    global Account acct {get; set;}
    global Lead lead {get; set;}

    global ApptSchedulerCon() {
        apptType = SALES;
        lstApptTypes = new List<String>{apptType};
        if (this instanceOf ApptSchedulerAcctCon) {
            return;
        }
        getResidence();
        initialize();
    }

    @RemoteAction
    global static  WestShoreUtils.RemoteActionResults changeDate(String jsonApptFilters, String selApptWrappersJSON) {
        System.debug('=========> jsonApptFilters = ' + jsonApptFilters);
        return BaseApptCon.applyFilters(jsonApptFilters, selApptWrappersJSON, true);
    }

    // apply filters so we only see Design Consultants who handle selected Product Group(s) in selected Region(s)
    @RemoteAction
    global static WestShoreUtils.RemoteActionResults applyFilters(String jsonApptFilters, String selApptWrappersJSON) {
        return BaseApptCon.applyFilters(jsonApptFilters, selApptWrappersJSON, false);
    }

    @RemoteAction
    global static WestShoreUtils.RemoteActionResults selectAppt(String selApptWrappersJSON, 
            String jsonApptSlot, String jsonApptFilters) {
        return BaseApptCon.selectAppt(selApptWrappersJSON, jsonApptSlot, jsonApptFilters, true);
    }

    // set the appointment the user has just selected
    @RemoteAction
    global static WestShoreUtils.RemoteActionResults setAppt(String jsonApptFilters, String selApptWrappersJSON) {
        return BaseApptCon.updateAppts(jsonApptFilters, selApptWrappersJSON);
    }
        
    public override void associateResidenceWithAppt(WestShoreUtils.AppointmentWrapper wrapper) {
        if (acct != null) {
            wrapper.apptSlot.Account__c = acct.Id;  
            wrapper.setResidence(acct);
        } else {
            wrapper.setResidence(lead);
            wrapper.apptSlot.Lead__c = lead.Id;         
        }
    }

    // sub-classes that extend standard controllers can provide their own implementations
    public virtual void getResidence() {
        // get Account if this Appointment is for an existing Customer
        residenceId = ApexPages.currentPage().getParameters().get(RESIDENCE_ID);
        if (residenceId.startsWith(ACCOUNT_PREFIX)) {
            acct = [select Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, 
                Region__c, Region__r.Name,
                (select Name, Phone from Contacts) 
                from Account where Id=:residenceId limit 1];
            System.debug('==========> Making appointment for Residence ' + acct.Name);
            residence = new Residence(acct);
        } else {
            // get Lead if this Appointment is for a new Customer (Lead)
            lead = [select Id, Name, Phone, Street, City, State, PostalCode, Region__c, Region__r.Name 
                from Lead where Id=:residenceId limit 1];
            System.debug('==========> Making appointment for Lead ' + lead.Name);
            residence = new Residence(lead);
        }
    }
}