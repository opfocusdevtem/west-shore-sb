/*
** Class:  ApptSchedulerAcctCon
** Created by OpFocus on 1/12/2016
** Description: Controller for the Appointment Scheduler VF Page for an Account
**  @See ApptSchedulerAcct VF page
*/  
global with sharing class ApptSchedulerAcctCon extends ApptSchedulerCon {

    static {
        BaseApptCon.myPage = Page.ApptSchedulerAcct;
    }

    global ApptSchedulerAcctCon(ApexPages.StandardController con) {
        residenceId = con.getId();
        getResidence();
        initialize();
    }

    @RemoteAction
    global static  WestShoreUtils.RemoteActionResults changeDateForAcct(String jsonApptFilters, String selApptWrappersJSON) {
        System.debug('=========> jsonApptFilters = ' + jsonApptFilters);
        return BaseApptCon.applyFilters(jsonApptFilters, selApptWrappersJSON, true);
    }

    // apply filters so we only see Design Consultants who handle selected Product Group(s) in selected Region(s)
    @RemoteAction
    global static WestShoreUtils.RemoteActionResults applyFiltersForAcct(String jsonApptFilters, String selApptWrappersJSON) {
        return BaseApptCon.applyFilters(jsonApptFilters, selApptWrappersJSON, false);
    }

    @RemoteAction
    global static WestShoreUtils.RemoteActionResults selectApptForAcct(String selApptWrappersJSON, 
            String jsonApptSlot, String jsonApptFilters) {
        return BaseApptCon.selectAppt(selApptWrappersJSON, jsonApptSlot, jsonApptFilters, true);
    }

    // set the appointment the user has just selected
    @RemoteAction
    global static WestShoreUtils.RemoteActionResults setApptForAcct(String jsonApptFilters, String selApptWrappersJSON) {
        return BaseApptCon.updateAppts(jsonApptFilters, selApptWrappersJSON);
    }


    public override void getResidence() {
        acct = [select Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, 
            Region__c, Region__r.Name,
            (select Name, Phone from Contacts) 
            from Account where Id=:residenceId limit 1];
        System.debug('==========> Making appointment for Residence ' + acct.Name);
        residence = new Residence(acct);
    }
}