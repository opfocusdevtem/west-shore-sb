/*
** Class:  ApptConfirmationCon
** Created by OpFocus on 11/18/2015
** Description: Controller for the Appointment Confirmation Page
** 	@See ApptConfirmation VF page
*/ 	
global with sharing class ApptConfirmationCon extends BaseApptCon {

	static {
		BaseApptCon.myPage = Page.ApptConfirmation;
	}

	global ApptConfirmationCon() {
		apptType = SALES;
		lstApptTypes = new List<String>{apptType};
		initialize();
	}

	@RemoteAction
	global static  WestShoreUtils.RemoteActionResults changeDate(String jsonApptFilters, String jsonSelApptWrappers) {
		return BaseApptCon.applyFilters(jsonApptFilters, jsonSelApptWrappers, true);
	}

    // apply filters so we only see Design Consultants who handle selected Product Group(s) in selected Region(s)
    @RemoteAction
    global static WestShoreUtils.RemoteActionResults applyFilters(String jsonApptFilters, String jsonSelApptWrappers) {
    	return BaseApptCon.applyFilters(jsonApptFilters, jsonSelApptWrappers, false);
    }

	@RemoteAction
    global static WestShoreUtils.RemoteActionResults selectAppt(String jsonSelApptWrappers, String jsonApptSlot,
           String jsonApptFilters) {
    	return BaseApptCon.selectAppt(jsonSelApptWrappers, jsonApptSlot, 
    		jsonApptFilters, false);
    }

	@RemoteAction
	global static WestShoreUtils.RemoteActionResults updateAppts(String jsonApptFilters, String jsonSelApptWrappers) {
		return BaseApptCon.updateAppts(jsonApptFilters, jsonSelApptWrappers);
	}
}