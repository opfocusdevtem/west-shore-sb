/*
** Class:  TestApptRemovalJob
** Created by OpFocus on 2/22/2016
** Description: Unit test for the Selected Appointment Removal Job that will periodically remove
**		Appointments in the past that have been orphaned (ie selected but never set/confirmed/staffed) 
**
*/ 	
@isTest
private class TestApptRemovalJob
{
	private static final Boolean testSuccess = true;
	private static final Boolean testScheduleExecute = true;

	@isTest
	static void testSuccess() {
		System.assert(testSuccess, 'Test Disabled');
		initData();
		Test.startTest();
			ApptRemovalJob job = new ApptRemovalJob();
			Database.executeBatch(job);
		Test.stopTest();
		System.assertEquals(0, [select Id from Appointment_Slot__c].size(), 
			'Failed to remove orphaned selected Appointments from the past.');
	}

	@isTest
	static void testScheduleExecute() {
		System.assert(testScheduleExecute, 'Test Disabled');
		initData();
		Test.startTest();
			ApptRemovalJob job = new ApptRemovalJob();
			job.addToStrErrMsg('Force an error message so the finish() method has something to do.');
			job.execute(null);
		Test.stopTest();
	}

	private static void initData() {
		// create some test appointments in the past for us to remove in batch job
		lstAppts = new List<Appointment_Slot__c>();

		Date yesterday = Date.today().addDays(-1);
		lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(yesterday, Time.newInstance(9,0,0,0)), 
			Status__c = 'Selected', Type_of_Appointment__c = 'Sales'));
		lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(yesterday, Time.newInstance(9,0,0,0)), 
			Status__c = 'Selected', Type_of_Appointment__c = 'Sales'));
		lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(yesterday, Time.newInstance(14,0,0,0)),
			Status__c = 'Selected', Type_of_Appointment__c = 'Sales'));
		lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(yesterday, Time.newInstance(18,0,0,0)),
			Status__c = 'Selected', Type_of_Appointment__c = 'Sales'));
		insert lstAppts;
	}

	private static List<Appointment_Slot__c> lstAppts;

}