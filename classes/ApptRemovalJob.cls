/*
** Class:  ApptRemovalJob
** Created by OpFocus on 2/22/2016
** Description: Job that will periodically (based on set schedule) remove Selected Appointments
**		in the past that have been orphaned (ie selected but never set/confirmed/staffed) 
**
** Code to schedule the job daily ad midnight:
	System.schedule('Appt Removal Job', '0 0 0 * * *', new ApptRemovalJob() );
**
** Code to run the job in 1 minute:
	Datetime nextRun = Datetime.now().addMinutes(1);
	String cronString = '' + nextRun.second() + ' ' + nextRun.minute() + ' ' + nextRun.hour() + ' ' + nextRun.day() + ' ' + nextRun.month() + ' ? ' + nextRun.year();
	System.schedule('Appt Removal Job (' + nextRun.getTime() + ')', cronString, new ApptRemovalJob());
**
** For more details on cron syntax, see:  http://www.quartz-scheduler.org/documentation/quartz-1.x/tutorials/crontrigger 
*/ 	
global class ApptRemovalJob implements Schedulable, Database.Batchable<sObject>, Database.Stateful {
	
	global String strErrMsg;

	global ApptRemovalJob() {		
	}
	
	// Start the scheduled job
	global void execute(SchedulableContext sc) {
		// If the AsyncApexJobs queue already has an active entry for this Apex class,
		// let it finish running - don't launch the batch job again
		Integer numRunning = 
		  [select count() 
		   from   AsyncApexJob 
		   where  ApexClass.Name = 'ApptRemovalJob' 
		   and    Status in ('Queued', 'Processing', 'Preparing', 'Holding')
		   and    JobType = 'Batch Apex'];
		if (numRunning > 0) {
			System.debug('==========> The ApptRemovalJob batch job is already running.');
			return;
		}
		
		Database.executeBatch(this);
	}

	// Start the batch job
	global Database.QueryLocator start(Database.BatchableContext BC) {
		// get all selected appointments from the past that we need to remove
		String soql = 'SELECT Id FROM Appointment_Slot__c ' +
			'WHERE Status__c=\'Selected\' AND Start_Time__c <  ' + 
			DateTime.now().format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
		return Database.getQueryLocator(soql);
	}

	// execute logic for next batch of records
   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		List<Appointment_Slot__c> lstApptsToRemove = (List<Appointment_Slot__c>)scope;
		Database.DeleteResult[] lstResults = Database.delete(lstApptsToRemove, false);

		// Find and report any errors 
		for (Integer i = 0; i<lstResults.size(); i++){
			if (lstResults[i].isSuccess() == false){
				// Report the error
				Integer idx = 0;
				String str = 'Unable to remove Orphaned Selected Appointment ' + lstApptsToRemove[i].Id + 
					'. Error: ' + lstResults[i].getErrors()[0].getMessage();
				System.debug('==========> ' + str);
				addToStrErrMsg(str + '\n');
			}
		}
	}
	
	// batch job finished - perform any cleanup if necessary
	global void finish(Database.BatchableContext BC) {
		// Get the results from Salesforce's AsyncApexJob object
		String strJobSummary = '';
		if (bc != null) {
			List<AsyncApexJob> lstJobResults = 
			  [select Status, TotalJobItems, JobItemsProcessed, NumberOfErrors, 
			  	JobType, ExtendedStatus, CompletedDate, ApexClassId 
			   from   AsyncApexJob
			   where  Id = :bc.getJobId()];

			if (!lstJobResults.isEmpty() && lstJobResults[0].NumberOfErrors > 0) {
				for (AsyncApexJob aaj : lstJobResults) strJobSummary += aaj + '\n';
				strJobSummary = '\n\n';
			}
		}
		
		if (strErrMsg != '') {
			strJobSummary += strErrMsg;
		}
		
		// if there are any errors, send the user executing the batch job an email with details
		if (strJobSummary != '') {
			User user = [select id, Email from User where Id = :UserInfo.getUserId()];

			Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
			email.setToAddresses(new List<String>{user.Email});
			email.setSubject('Error in scheduled job to remove Selected Appointments in the past');
			email.setPlainTextBody('The scheduled job to remove Selected Appointments in the past has ' +
				 ' encountered the following error(s):\n\n' + strJobSummary);
			Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});
		}
	}


	// ------------------------------------------------------------------------------------------
	// Add the given String to strErrMsg. If that would cause us to come close to the heap size, though, don't.
	@TestVisible private void addToStrErrMsg(String msg) {
		Integer maxHeapSize = Limits.getLimitHeapSize();
		Integer currentHeapSize = Limits.getHeapSize();
		Integer almostMaxHeapSize = maxHeapSize - 2048;
		if (currentHeapSize <= almostMaxHeapSize) strErrMsg += msg;
	}
}