/*
** Class:  TestApptSchedulerCon
** Created by OpFocus on 10/26/2015
** Description: Unit test for Appointment Scheduler Controller
*/  
@isTest
private class TestApptSchedulerCon {

    private static final Boolean testInit = true;
    private static final Boolean testSelectAndSetApptForAcct = true;
    private static final Boolean testSelectApptWithAddlTeam = true;
    private static final Boolean testChangeDate = true;
    private static final Boolean testConflictingAppts = true;
    private static final Boolean testApplyFilters = true;
    private static final Boolean testApplyingFilters = true;
    private static final Boolean testRemoveAppts = true;
    private static final Boolean testBuildWrappers = true;
    private static final Boolean testBuildUrlParam = true;

    // Add Tests for the following conditions
    // 1. Attempt to staff an appointment that has SetUnassigned Row for Requested Team
    // 2. Attempt to staff an appointment that has ConfirmedUnassigned Row for Requested Team
    // 3. Update an Appointment that has ATTEMPTED TO CONTACT status
    // 4. Update an Appointment with a status that is not Set, Confirmed, or Staffed
    // 5. Attempt to staff an appointment without a Team specification
    // 6. Create an Appointment with an Additional Team for a User
    @isTest
    static void testInit() {
        System.assert(testInit, 'Test Disabled.');
        TestApptHelper.initData(false, TestApptHelper.SCHEDULER, Page.ApptScheduler);

        Test.setCurrentPageReference(Page.ApptScheduler);
        ApexPages.currentPage().getParameters().put('id', TestApptHelper.lead.Id);

        Test.startTest();
            TestApptHelper.con = new ApptSchedulerCon();
        Test.stopTest();

        // verify that the correct number of appointment slots were added to controller
        System.assertEquals(5, TestApptHelper.con.apptRows.size(), 
            'Should have been 1 row created for each team tember and there are 5 teams.');

        // as there are no conflicting events, each team member should have a single row
        System.assertEquals(1, 
            TestApptHelper.con.apptRows.get(TestApptHelper.mapTeamsByType.get('Sales').Id).size(), 
            'Incorrect number of Appointment Rows created for Sales Team.');
    }

    @isTest
    static void testChangeDate() {
        System.assert(testChangeDate, 'Test Disabled.');
        TestApptHelper.initData(false, TestApptHelper.SCHEDULER, Page.ApptScheduler);

        TestApptHelper.testChangeDate(TestApptHelper.SCHEDULER, Page.ApptScheduler, 'Sales');
    }

    @isTest
    static void testSelectAndSetApptForAcct() {
        System.assert(testSelectAndSetApptForAcct, 'Test Disabled.');
        TestApptHelper.initData(true, TestApptHelper.SCHEDULER, Page.ApptScheduler);
        TestApptHelper.testSelectAppt(TestApptHelper.SCHEDULER, Page.ApptScheduler, 
        	WestShoreUtils.SETT, 'Setting');
    }

    @isTest
    static void testSelectApptWithAddlTeam() {
        System.assert(testSelectApptWithAddlTeam, 'Test Disabled.');
        TestApptHelper.initData(true, TestApptHelper.SCHEDULER, Page.ApptScheduler);

        WestShoreUtils.AppointmentWrapper apptDetails = new WestShoreUtils.AppointmentWrapper();
        WestShoreUtils.EventWrapper wrapper = 
            TestApptHelper.con.apptRows.get(TestApptHelper.mapTeamsByType.get('Sales').Id).get(0).appts.get(0);
        apptDetails.id = ((WestShoreUtils.AppointmentWrapper)wrapper).Id;
        apptDetails.selDate = Date.today().format();
        apptDetails.startTime = '9:30';
        apptDetails.duration = 120;
        apptDetails.residenceId = TestApptHelper.lead.Id;
        apptDetails.status = 'Available';
        apptDetails.notes = 'test notes';
        apptDetails.type = wrapper.type;
        apptDetails.subType = wrapper.subType;
        apptDetails.team = wrapper.team;
        apptDetails.addlTeam = TestApptHelper.mapTeamsByType.get('Install').Id;
        apptDetails.addlTeamName = 'Install Team';
        apptDetails.buffer = 90;
        apptDetails.requestedStatus = WestShoreUtils.SETT;
        apptDetails.requestedStatusAction = 'Setting';
        apptDetails.productGroup = 'Windows & Doors';
        apptDetails.attemptedToContact = false;
        apptDetails.numQuarterHours = 14;
        apptDetails.isOneLegger = false;
        apptDetails.numPeople = 1;
        apptDetails.needToBeHome = false;
        apptDetails.name = 'name';
        apptDetails.region = TestApptHelper.pittsburgh.Id;
        apptDetails.regionName = TestApptHelper.pittsburgh.Name;
        

        WestShoreUtils.AppointmentFilters filters = new WestShoreUtils.AppointmentFilters();
        filters.productGroup = 'All';
        filters.region = 'All';
        filters.residenceId = TestApptHelper.lead.Id;
        filters.apptTypes = 'Install';
        filters.startDate = Date.today().format();

        WestShoreUtils.RemoteActionResults results = null;
        Test.startTest();
            results = ApptSchedulerCon.selectAppt(JSON.serialize(new List<BaseApptCon.SelectedApptWrapper>()), 
                JSON.serialize(apptDetails), 
                JSON.serialize(filters));
        Test.stopTest();
        System.assertEquals(true, results.isSuccess, 
            'Did not select Appointment correctly as expected. Error Details = ' + results.details);

        System.assertEquals(apptDetails.addlTeam, 
            [select Additional_Team__c from Appointment_Slot__c 
            where Status__c=:WestShoreUtils.SELECTED limit 1].Additional_Team__c, 
            'Did not set Additional Team as expected.');
    }
    

    @isTest
    static void testApplyFilters() {
        System.assert(testApplyFilters, 'Test Disabled.');
        TestApptHelper.initData(true, TestApptHelper.SCHEDULER, Page.ApptScheduler);
        TestApptHelper.testApplyFilters(TestApptHelper.SCHEDULER, Page.ApptScheduler);
    }

    @isTest
    static void testApplyingFilters() {
        System.assert(testApplyingFilters, 'Test Disabled.');
        TestApptHelper.initData(false, TestApptHelper.SCHEDULER, Page.ApptScheduler);

        String prodGrp = 'Windows & Doors';

        Date myDate = Date.today();
        Region__c london = new Region__c(Name='London, England');
        Region__c paris = new Region__c(Name='Paris, France');
        Region__c florence = new Region__c(Name='Florence, Italy');
        insert new List<Region__c> {london, paris, florence};
        
        // now lets' update our Team to have the same Product Group & Region values
        Team__c ourteam = TestApptHelper.mapTeamsByType.get('Sales');
        ourTeam.Product_Group__c = 'Showers & Baths';
        ourTeam.Region__c = florence.Id;
        update ourTeam;

        // now construct controller with filter values
        PageReference testPage = Page.ApptScheduler;
        Test.setCurrentPageReference(testPage);
        ApexPages.currentPage().getParameters().put('prodGrp', prodGrp);
        ApexPages.currentPage().getParameters().put('region', london.Id);
        ApexPages.currentPage().getParameters().put('id', TestApptHelper.acct.Id);
        Test.startTest();
            TestApptHelper.con = new ApptSchedulerCon();
        Test.stopTest();

        // there should only be no TeamAppointmentRow entries in our map as none of our teams 
        // are in London region nor do they service our prodGrp
        System.assertEquals(0, TestApptHelper.con.apptRows.size(), 
            'Failed to filter teams by product group & region.');
    }

    @isTest
    static void testRemoveAppts() {
        System.assert(testRemoveAppts, 'Test Disabled.');
        Date myDate = Date.today();
        TestApptHelper.initData(false, TestApptHelper.SCHEDULER, Page.ApptScheduler);
        Appointment_Slot__c appt = new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(14,0,0,0)),
            Team__c = TestApptHelper.mapTeamsByType.get('Sales').Id, Account__c = TestApptHelper.acct.Id,
            Status__c = 'Selected', Type_of_Appointment__c = 'Sales', 
            Product_Group__c = 'Showers & Baths',
            Duration__c=60, Region__c=TestApptHelper.pittsburgh.Id);
        insert appt;
        List<BaseApptCon.SelectedApptWrapper> lstWrappers = new List<BaseApptCon.SelectedApptWrapper>();
        lstWrappers.add(new BaseApptCon.SelectedApptWrapper(appt.Id, WestShoreUtils.SETT, null, 'Sales',
                TestApptHelper.pittsburgh.Id));
        Test.startTest();
            BaseApptCon.removeSelectedAppts(lstWrappers);
        Test.stopTest();
        System.assertEquals(0, [select Id from Appointment_Slot__c where Id=:appt.Id].size(), 
            'did not remove Appointment slot as expected.');
    }

    @isTest
    static void testBuildWrappers() {
        System.assert(testBuildWrappers, 'Test Disabled.');
        String apptType = 'Sales';
        TestApptHelper.initData(true, TestApptHelper.SCHEDULER, Page.ApptScheduler);
        // url contains duplicate Ids
        String url = '';

        Date myDate = Date.today();
        // now create some appointments
        List<Appointment_Slot__c> lstAppts = new List<Appointment_Slot__c>();
        Id teamId = TestApptHelper.mapTeamsByType.get(apptType).Id;

        lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(9,00,0,0)), 
            Team__c = teamId, Account__c = TestApptHelper.acct.Id, 
            Status__c = 'Selected', Type_of_Appointment__c = apptType, Duration__c=180));

        lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(9,00,0,0)), 
            Team__c = teamId, Account__c = TestApptHelper.acct.Id, 
            Status__c = 'Selected', Type_of_Appointment__c = apptType, Duration__c=180));
                
        // should not be included as it is no longer selected
        lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(9,00,0,0)), 
            Team__c = teamId, Account__c = TestApptHelper.acct.Id, 
            Status__c = 'Set', Type_of_Appointment__c = apptType, Duration__c=180));

        // should not be included as it is no longer selected
        lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(9,00,0,0)), 
            Team__c = teamId, Account__c = TestApptHelper.acct.Id, 
            Status__c = 'Selected', Type_of_Appointment__c = apptType, Duration__c=180));
                
        insert lstAppts;

        for (Appointment_SLot__c appt : lstAppts) {
            url += appt.Id + ':' + appt.Status__c + ':' + '1111,';
        }
        url += lstAppts.get(0).Id + ':' + 'NextStatus'+ ':' + '2222';
        delete lstAppts.get(1);
        List<BaseApptCon.SelectedApptWrapper> lstWrappers = null;

        Test.startTest();
            lstWrappers = TestApptHelper.con.buildWrappers(url);
        Test.stopTest();
        System.assertEquals(2, lstWrappers.size(), 'Created wrong number of wrappers for duplicate id.');
    }

    @isTest
    static void testBuildUrlParam() {
        System.assert(testBuildUrlParam, 'Test Disabled.');
        String apptType = 'Sales';
        TestApptHelper.initData(true, TestApptHelper.SCHEDULER, Page.ApptScheduler);
        
        Date myDate = Date.today();
        // now create some appointments
        List<Appointment_Slot__c> lstAppts = new List<Appointment_Slot__c>();

        Id teamId = TestApptHelper.mapTeamsByType.get(apptType).Id;

        lstAppts.add(new Appointment_Slot__c(Name='DupeMe', Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(9,00,0,0)), 
            Team__c = teamId, Account__c = TestApptHelper.acct.Id, 
            Status__c = 'Selected', Type_of_Appointment__c = apptType, Duration__c=180));
        lstAppts.add(new Appointment_Slot__c(Name='KeepMe', Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(9,00,0,0)), 
            Team__c = teamId, Account__c = TestApptHelper.acct.Id, 
            Status__c = 'Selected', Type_of_Appointment__c = apptType, Duration__c=180));
                
        // should not be included as it is no longer selected
        lstAppts.add(new Appointment_Slot__c(Name='NotSelected',Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(9,00,0,0)), 
            Team__c = teamId, Account__c = TestApptHelper.acct.Id, 
            Status__c = 'Set', Type_of_Appointment__c = apptType, Duration__c=180));

        // should not be included as it is no longer selected
        lstAppts.add(new Appointment_Slot__c(Name='DeleteMe', Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(9,00,0,0)), 
            Team__c = teamId, Account__c = TestApptHelper.acct.Id, 
            Status__c = 'Selected', Type_of_Appointment__c = apptType, Duration__c=180));
                
        insert lstAppts;

        List<BaseApptCon.SelectedApptWrapper> lstWrappers = new List<BaseApptCon.SelectedApptWrapper>();
        String expectedResult = '';
        Appointment_SLot__c apptToDelete = null;

        for (Appointment_SLot__c appt : lstAppts) {
            lstWrappers.add(new BaseApptCon.SelectedApptWrapper(appt.Id, appt.Status__c, teamId, apptType,
                 TestApptHelper.pittsburgh.Id));
            if (appt.Name.equals('DeleteMe')) {
                apptToDelete = appt;
                continue;
            }
            if (appt.Name.equals('DupeMe')) {
                lstWrappers.add(new BaseApptCon.SelectedApptWrapper(appt.Id, appt.Status__c, teamId, apptType,
                    TestApptHelper.pittsburgh.Id));
            }
            if (appt.Name.equals('NotSelected')) {
                continue;
            }
            expectedResult += appt.Id + ':' + appt.Status__c + ':' + TestApptHelper.pittsburgh.Id + ':' + teamId + ',';
        }
        expectedResult = expectedResult.substringBeforeLast(',');
        delete apptToDelete;

        String url = '';
        Test.startTest();
            url = BaseApptCon.buildUrlParam(lstWrappers);
        Test.stopTest();
        System.assertEquals(expectedResult, url, 'Created incorrect URL from wrappers.');
    }
    
    @isTest
    static void testConflictingAppts() {
        System.assert(testConflictingAppts, 'Test Disabled.');
        String apptType = 'Sales';
        TestApptHelper.initData(false, TestApptHelper.SCHEDULER, Page.ApptScheduler);

        Date myDate = Date.today();
        // now create some conflicting appointments
        List<Appointment_Slot__c> lstAppts = new List<Appointment_Slot__c>();
        Id teamId = TestApptHelper.mapTeamsByType.get(apptType).Id;

        // 9:00 - 12:00 + buffer = 13:30
        lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(9,00,0,0)), 
            Team__c = teamId, Account__c = TestApptHelper.acct.Id, 
            Status__c = 'Set', Type_of_Appointment__c = apptType, Duration__c=180));
        
        // 9:00 - 12:00 + buffer = 13:30
        lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(9,00,0,0)), 
            Team__c = teamId, Account__c = TestApptHelper.acct.Id, 
            Status__c = 'Set', Type_of_Appointment__c = apptType, Duration__c=180));
        
        // 10:00 - 11:00 + buffer : 13:30
        lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(10,0,0,0)),
            Team__c = teamId, Account__c = TestApptHelper.acct.Id,
            Status__c = 'Set', Type_of_Appointment__c = apptType, Duration__c=60));

        // 11:00 - 12:30 + buffer = 14:00
        lstAppts.add(new Appointment_Slot__c(Start_Time__c = DateTime.newInstance(myDate, 
            Time.newInstance(11,0,0,0)),
            Team__c = teamId, Account__c = TestApptHelper.acct.Id,
            Status__c = 'Set', Type_of_Appointment__c = apptType,  Duration__c=90));

        insert lstAppts;

        Test.setCurrentPageReference(Page.ApptScheduler);
        ApexPages.currentPage().getParameters().put('id', TestApptHelper.acct.Id);

        Test.startTest();
            TestApptHelper.con = new ApptSchedulerCon();
        Test.stopTest();

        // there should have been 4 rows created for this team.
        System.assertEquals(4, 
            TestApptHelper.con.apptRows.get(TestApptHelper.mapTeamsByType.get(apptType).Id).size(), 
            'Failed to create additional rows when there are conflicting events.');
    }
}